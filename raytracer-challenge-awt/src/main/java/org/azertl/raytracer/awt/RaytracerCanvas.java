package org.azertl.raytracer.awt;

import org.azertl.raytracer.model.Color;

import java.awt.Canvas;
import java.awt.Graphics;

public class RaytracerCanvas extends Canvas {
    private org.azertl.raytracer.model.Canvas raytracerCanvas;

    public RaytracerCanvas(org.azertl.raytracer.model.Canvas raytracerCanvas) {
        this.raytracerCanvas = raytracerCanvas;
    }

    @Override
    public void paint(Graphics g) {
        for (int i = 0; i < raytracerCanvas.width; i++) {
            for (int j = 0; j < raytracerCanvas.height; j++) {
                Color raytracerColor = raytracerCanvas.getPixel(i, j);

                g.setColor(new java.awt.Color(
                        Math.min((float) raytracerColor.red(), 1),
                        Math.min((float) raytracerColor.green(), 1),
                        Math.min((float) raytracerColor.blue(), 1)));
                g.drawLine(i, j, i, j);
            }
        }
    }
}
