package org.azertl.raytracer.awt;

import javax.swing.JFrame;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.HeadlessException;

public class RaytracerFrame extends JFrame {

    public RaytracerFrame(org.azertl.raytracer.model.Canvas raytracerCanvas) throws HeadlessException {
        super("Raytracer");

        // create a empty canvas
        Canvas c = new RaytracerCanvas(raytracerCanvas);

        // set background
        c.setBackground(Color.black);

        add(c);
        setSize(400, 300);
    }
}
