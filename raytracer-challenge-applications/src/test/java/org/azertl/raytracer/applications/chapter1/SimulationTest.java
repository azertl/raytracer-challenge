package org.azertl.raytracer.applications.chapter1;

import org.assertj.core.api.Assertions;
import org.azertl.raytracer.applications.chapter01.Simulation;
import org.junit.jupiter.api.Test;

public class SimulationTest {
    @Test
    public void given_params_then_simulation_ends_correctly() {
        Simulation sim = new Simulation(Simulation.ENVIRONMENT, Simulation.PROJECTILE);
        sim.run();

        Assertions.assertThat(sim.projectile.getPosition().y).isLessThan(0);
    }
}
