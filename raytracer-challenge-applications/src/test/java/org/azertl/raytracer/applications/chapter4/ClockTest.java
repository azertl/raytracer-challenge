package org.azertl.raytracer.applications.chapter4;

import org.assertj.core.api.Assertions;
import org.azertl.raytracer.applications.chapter04.Clock;
import org.azertl.raytracer.model.Canvas;
import org.azertl.raytracer.model.Color;
import org.junit.jupiter.api.Test;

public class ClockTest {

    @Test
    public void testClock() {
        Clock clock = new Clock();
        clock.generate();

        Canvas canvas = clock.getCanvas();
        assertPointIsWhite(canvas, 31,84);
        assertPointIsWhite(canvas, 16,70);
        assertPointIsWhite(canvas, 10,50);
    }

    private void assertPointIsWhite(Canvas canvas, int i,int j) {
        Color white = new Color(1,1,1);
        Color black= new Color(0,0,0);
        Assertions.assertThat(canvas.getPixel(i, j)).isEqualTo(white);

        Assertions.assertThat(canvas.getPixel(i-1, j-1)).isEqualTo(black);
        Assertions.assertThat(canvas.getPixel(i-1, j-1)).isEqualTo(black);
        Assertions.assertThat(canvas.getPixel(i+1, j-1)).isEqualTo(black);

        Assertions.assertThat(canvas.getPixel(i-1, j)).isEqualTo(black);
        Assertions.assertThat(canvas.getPixel(i+1, j)).isEqualTo(black);

        Assertions.assertThat(canvas.getPixel(i-1, j+1)).isEqualTo(black);
        Assertions.assertThat(canvas.getPixel(i-1, j+1)).isEqualTo(black);
        Assertions.assertThat(canvas.getPixel(i+1, j+1)).isEqualTo(black);
    }
}
