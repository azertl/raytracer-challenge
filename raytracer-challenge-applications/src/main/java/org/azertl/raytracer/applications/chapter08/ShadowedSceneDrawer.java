package org.azertl.raytracer.applications.chapter08;

import org.azertl.raytracer.applications.AbstractWorldDrawer;
import org.azertl.raytracer.model.Camera;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Material;
import org.azertl.raytracer.model.PointLight;
import org.azertl.raytracer.model.Sphere;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.operations.Transformations;

public class ShadowedSceneDrawer extends AbstractWorldDrawer {
    public ShadowedSceneDrawer() {
        super(true);
    }

    public static void main(String[] args) {
        ShadowedSceneDrawer sceneDrawer = new ShadowedSceneDrawer();
        sceneDrawer.generate();
        sceneDrawer.displayFrame();
    }

    @Override
    protected void populateWorld() {
        // wall
        Sphere wall = getWorld().createSphere();
        wall.setTransform(new Transformation()
                .scaling(100, 100, 0.01)
                .rotationX(Math.PI / 4)
                .translation(0,10,-10)
        );

        Material material = wall.getMaterial();
        material.setColor(0.5, 0.5, 0.5);
        material.setSpecular(0);

        // A cyan sphere
        Sphere sphere = getWorld().createSphere();
        sphere.getMaterial().setColor(0, 1, 1);

        // A blue sphere : the head
        sphere = getWorld().createSphere();
        sphere.getMaterial().setColor(0, 0, 1);
        sphere.setTransform(
                new Transformation()
                        .scaling(1,2,0.1)
                        .rotationX(Math.PI/4)
                        .translation(1,2,0)
        );

        // A blue sphere : the ears
        sphere = getWorld().createSphere();
        sphere.getMaterial().setColor(0, 0, 1);
        sphere.setTransform(
                new Transformation()
                        .scaling(0.5,0.75,0.1)
                        .rotationZ(Math.PI/2)
                        .rotationX(Math.PI/4)
                        .translation(2,1.75,-0.5)
        );

        // A blue sphere : the jaw
        sphere = getWorld().createSphere();
        sphere.getMaterial().setColor(0, 0, 1);
        sphere.setTransform(
                new Transformation()
                        .scaling(0.2,1,0.1)
                        .rotationX(Math.PI/4)
                        .rotationY(-Math.PI/8)
                        .translation(0.25,1.75,1)
        );


        // light source is white, shining from the left
        PointLight lightSource = new PointLight(Tuple.point(0, -50, 50), new Color(1, 1, 1));
        getWorld().setLightSource(lightSource);
    }

    @Override
    protected Camera initCamera() {
        Camera camera = new Camera(400, 200, Math.PI / 2);
        camera.setTransform(Transformations.viewTransform(
                Tuple.point(0, 0, 10), Tuple.point(0, 5, 0), Tuple.vector(1, 0, 0)));
        return camera;
    }
}
