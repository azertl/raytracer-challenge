package org.azertl.raytracer.applications;

import org.azertl.raytracer.model.Camera;
import org.azertl.raytracer.model.World;

public abstract class AbstractWorldDrawer extends AbstractCanvasDrawer {
    private World world;

    protected boolean allowShadows;

    protected AbstractWorldDrawer(boolean allowShadows) {
        this.allowShadows = allowShadows;
    }

    protected void initWorld() {
        world = new World();
        world.setAllowShadows(allowShadows);
    }

    protected abstract void populateWorld();

    protected abstract Camera initCamera();

    public void generate() {
        initWorld();
        populateWorld();

        Camera camera = initCamera();

        canvas = camera.render(world, true);
    }

    public World getWorld() {
        return world;
    }
}
