package org.azertl.raytracer.applications.chapter05;

import org.azertl.raytracer.applications.AbstractCanvasDrawer;
import org.azertl.raytracer.model.*;

public class SphereDrawer extends AbstractCanvasDrawer {

    private Color red = new Color(1, 0, 0);
    private Color white= new Color(1, 1, 1);

    public void generate() {
        Sphere sphere = initSphere();

        for (int i = 0; i < canvas.width; i++) {
            for (int j = 0; j < canvas.height; j++) {
                Ray ray = getMovingDirectionRay(i, j);

                Intersection intersection = sphere.intersect(ray).hit();

                if (intersection != null) {
                    canvas.setPixel(i, j, red);
                } else {
                    canvas.setPixel(i, j, white);
                }
            }
        }
    }

    /**
     * Cast a ray whose origin depends on i and j. The direction never changes
     */
    private static Ray getMovingOriginRay(int i, int j) {
        return new Ray(Tuple.point(i, j, 2), Tuple.vector(0, 0, -1));
    }

    /**
     * Cast a ray whose origin never changes but is directed to a given point of the canvas
     */
    private Ray getMovingDirectionRay(int i, int j) {
        Tuple canvasPos = Tuple.point(i,j,0);
        Tuple rayOrigin = Tuple.point(50, 50, 100);
        return new Ray(rayOrigin, canvasPos.subtract(rayOrigin));
    }

    private Sphere initSphere() {
        // Init sphere
        Sphere sphere = new Sphere();

        // Transformation applied to sphere
        Transformation transformation = new Transformation();
        transformation.scaling(30, 30, 30);
        transformation.translation(50, 50, 0);

        sphere.setTransform(transformation);
        return sphere;
    }

    public static void main(String[] args) {
        SphereDrawer sphereDrawer = new SphereDrawer();
        sphereDrawer.generate();
        sphereDrawer.save();
    }
}
