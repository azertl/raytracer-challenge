package org.azertl.raytracer.applications.chapter10;

import org.azertl.raytracer.applications.AbstractWorldDrawer;
import org.azertl.raytracer.model.Camera;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Plane;
import org.azertl.raytracer.model.PointLight;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.model.patterns.NestedPattern;
import org.azertl.raytracer.model.patterns.StripePattern;

public class NestedPatternDrawer extends AbstractWorldDrawer {
    protected NestedPatternDrawer() {
        super(true);
    }

    @Override
    protected void populateWorld() {
        Plane hPlane = getWorld().createPlane();
        hPlane.getMaterial().setPattern(
                new NestedPattern(
                        new StripePattern(
                                new Color(1, 1, 0), new Color(0, 1, 0))
                                .setTransform(
                                        new Transformation()
                                                .rotationY(Math.PI)
                                                .scaling(0.125,0.125,0.125)
                                ),
                        new StripePattern(
                                new Color(0, 0, 1), new Color(1, 0, 0))
                                .setTransform(
                                        new Transformation()
                                                .rotationY(Math.PI / 2)
                                                .scaling(0.125,0.125,0.125)
                                )
                ));

        getWorld().setLightSource(new PointLight(Tuple.point(10, 10, 10), new Color(1, 1, 1)));
    }


    @Override
    protected Camera initCamera() {
        Camera camera = new Camera(400, 200, Math.PI / 2);

        camera.setViewTransform(Tuple.point(0, 4, 20), Tuple.point(0, 0, 0), Tuple.vector(0, 1, 0));

        return camera;
    }

    public static void main(String[] args) {
        NestedPatternDrawer patternDrawer = new NestedPatternDrawer();
        patternDrawer.generate();
        patternDrawer.displayFrame();
    }
}
