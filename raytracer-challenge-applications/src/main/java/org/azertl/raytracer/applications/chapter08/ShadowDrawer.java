package org.azertl.raytracer.applications.chapter08;

import org.azertl.raytracer.applications.chapter07.SceneDrawer;

public class ShadowDrawer extends SceneDrawer {
    public ShadowDrawer() {
        super(true);
    }

    public static void main(String[] args) {
        ShadowDrawer shadowDrawer = new ShadowDrawer();
        shadowDrawer.generate();
        shadowDrawer.displayFrame();
    }
}
