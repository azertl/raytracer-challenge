package org.azertl.raytracer.applications.chapter10;

import org.azertl.raytracer.applications.AbstractWorldDrawer;
import org.azertl.raytracer.model.*;
import org.azertl.raytracer.model.patterns.CheckersPattern;
import org.azertl.raytracer.model.patterns.GradientPattern;
import org.azertl.raytracer.model.patterns.RadialGradientPattern;
import org.azertl.raytracer.model.patterns.RingPattern;
import org.azertl.raytracer.model.patterns.StripePattern;

public class PatternDrawer extends AbstractWorldDrawer {
    protected PatternDrawer() {
        super(true);
    }

    @Override
    protected void populateWorld() {
        Plane hPlane = getWorld().createPlane();
        hPlane.getMaterial().setPattern(
                new GradientPattern(new Color(1, 1, 1), new Color(0, 0, 0))
                        .setTransform(new Transformation().scaling(2, 2, 2))
        );

        Plane vPlane = getWorld().createPlane();
        vPlane.setTransform(new Transformation().rotationZ(Math.PI / 3).translation(10, 0, 0));
        vPlane.getMaterial().setPattern(
                new StripePattern(new Color(0, 0, 1), new Color(1, 0, 0))
                        .setTransform(new Transformation().rotationX(Math.PI / 3))
        );

        getWorld().createSphere()
                .setPattern(new CheckersPattern(
                        new Color(1, 1, 0), new Color(0, 1, 0))
                        .setTransform(
                                new Transformation()
                                        .translation(0.5, 0.5, 0.5)
                                        .rotationY(Math.PI/4)
                        )
                )
                .setTransform(new Transformation().scaling(3, 3, 3).translation(0, 3, 0));

        getWorld().createSphere()
                .setPattern(new RingPattern(
                        new Color(1, 1, 0), new Color(0, 1, 0))
                        .setTransform(
                                new Transformation()
                                        .translation(0.5, 0.5, 0.5)
                                        .rotationY(Math.PI/4)
                        ))
                .setTransform(new Transformation().translation(5, 1, 3));

        getWorld().createSphere()
                .setPattern(new RadialGradientPattern(
                        new Color(1, 0, 0), new Color(0, 0, 1))
                        .setTransform(
                                new Transformation()
                                        .translation(0.5, 0.5, 0.5)
                                        .rotationY(Math.PI/4)
                        ))
                .setTransform(new Transformation().scaling(1.5, 1.5, 1.5).translation(-6, 1.5, 3));

        getWorld().setLightSource(new PointLight(Tuple.point(10, 10, 10), new Color(1, 1, 1)));
    }

    @Override
    protected Camera initCamera() {
        Camera camera = new Camera(400, 200, Math.PI / 2);

        camera.setViewTransform(Tuple.point(0, 4, 20), Tuple.point(0, 0, 0), Tuple.vector(0, 1, 0));

        return camera;
    }

    public static void main(String[] args) {
        PatternDrawer patternDrawer = new PatternDrawer();
        patternDrawer.generate();
        patternDrawer.displayFrame();
    }
}
