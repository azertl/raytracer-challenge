package org.azertl.raytracer.applications.chapter06;

import org.azertl.raytracer.applications.AbstractCanvasDrawer;
import org.azertl.raytracer.model.*;

public class LightDrawer extends AbstractCanvasDrawer {

    public void generate() {
        Sphere sphere = initSphere();

        PointLight pointLight = initLight();

        for (int i = 0; i < canvas.width; i++) {
            for (int j = 0; j < canvas.height; j++) {
                Ray ray = getMovingDirectionRay(i, j);

                Intersection intersection = sphere.intersect(ray).hit();

                if (intersection != null) {
                    Tuple point = ray.getPosition(intersection.getTime());
                    Sphere object = (Sphere) intersection.getObject();
                    Tuple normal = object.getNormalAt(point);
                    Tuple eye = ray.getDirection().negate();

                    Color color = object.getMaterial().lighting(pointLight, object, point, eye, normal, false);

                    canvas.setPixel(i, j, color);
                }
            }
        }
    }

    private PointLight initLight() {
        Tuple position = Tuple.point(-30, -20, 20);
        Color intensity = new Color(1, 1, 1);
        return new PointLight(position, intensity);
    }

    /**
     * Cast a ray whose origin never changes but is directed to a given point of the canvas
     */
    private Ray getMovingDirectionRay(int i, int j) {
        Tuple canvasPos = Tuple.point(i, j, 0);
        Tuple rayOrigin = Tuple.point(50, 50, 100);
        return new Ray(rayOrigin, canvasPos.subtract(rayOrigin).normalize());
    }

    private Sphere initSphere() {
        // Init sphere
        Sphere sphere = new Sphere();

        // Give it a material
        Material material = sphere.getMaterial();
        material.setColor(1, 0.2, 1);


        // Transformation applied to sphere
        Transformation transformation = new Transformation();
        transformation.scaling(30, 30, 30);
        transformation.translation(50, 50, 0);

        sphere.setTransform(transformation);
        return sphere;
    }

    public static void main(String[] args) {
        LightDrawer sphereDrawer = new LightDrawer();
        sphereDrawer.generate();
        sphereDrawer.displayFrame();
    }
}
