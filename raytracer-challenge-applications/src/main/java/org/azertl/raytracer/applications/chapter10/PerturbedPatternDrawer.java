package org.azertl.raytracer.applications.chapter10;

import org.azertl.raytracer.applications.AbstractWorldDrawer;
import org.azertl.raytracer.model.Camera;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Plane;
import org.azertl.raytracer.model.PointLight;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.model.patterns.PerturbedPattern;
import org.azertl.raytracer.model.patterns.StripePattern;

public class PerturbedPatternDrawer extends AbstractWorldDrawer {
    protected PerturbedPatternDrawer() {
        super(true);
    }

    @Override
    protected void populateWorld() {
        getWorld().createSphere()
                .setPattern(
                        new PerturbedPattern(
                                new StripePattern(
                                        new Color(1, 0, 0), new Color(0, 0, 1)
                                )

                        )
                )
                .setTransform(new Transformation().scaling(5, 5, 5).rotationY(Math.PI/10).translation(0, 3, 4));

        getWorld().setLightSource(new PointLight(Tuple.point(10, 10, 10), new Color(1, 1, 1)));
    }


    @Override
    protected Camera initCamera() {
        Camera camera = new Camera(400, 200, Math.PI / 2);

        camera.setViewTransform(Tuple.point(0, 4, 20), Tuple.point(0, 0, 0), Tuple.vector(0, 1, 0));

        return camera;
    }

    public static void main(String[] args) {
        PerturbedPatternDrawer patternDrawer = new PerturbedPatternDrawer();
        patternDrawer.generate();
        patternDrawer.displayFrame();
    }
}
