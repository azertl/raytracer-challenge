package org.azertl.raytracer.applications.chapter09;

import org.azertl.raytracer.applications.AbstractWorldDrawer;
import org.azertl.raytracer.model.Camera;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.PointLight;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;

public class PlanesDrawer extends AbstractWorldDrawer {
    protected PlanesDrawer() {
        super(true);
    }

    @Override
    protected void populateWorld() {
        getWorld().createPlane();

        getWorld().createSphere()
                .setColor(new Color(0, 1, 1))
                .setTransform(new Transformation().scaling(3, 3, 3).translation(0, 3, 0));

        getWorld().createSphere()
                .setColor(new Color(1, 1, 0))
                .setTransform(new Transformation().translation(5, 1, 3));

        getWorld().createSphere()
                .setColor(new Color(0.2, 1, 0.2))
                .setTransform(new Transformation().scaling(1.5, 1.5, 1.5).translation(-6, 1.5, 3));

        getWorld().setLightSource(new PointLight(Tuple.point(10, 10, 10), new Color(1, 1, 1)));
    }

    @Override
    protected Camera initCamera() {
        Camera camera = new Camera(400, 200, Math.PI / 2);

        camera.setViewTransform(Tuple.point(0, 4, 20), Tuple.point(0, 0, 0), Tuple.vector(0, 1, 0));

        return camera;
    }

    public static void main(String[] args) {
        PlanesDrawer planesDrawer = new PlanesDrawer();
        planesDrawer.generate();
        planesDrawer.displayFrame();
    }
}
