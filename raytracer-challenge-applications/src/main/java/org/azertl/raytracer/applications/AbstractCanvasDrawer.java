package org.azertl.raytracer.applications;

import org.azertl.raytracer.awt.RaytracerFrame;
import org.azertl.raytracer.model.Canvas;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.utils.PpmBuilder;

import javax.swing.JFrame;

public abstract class AbstractCanvasDrawer {

    protected Canvas canvas;

    protected AbstractCanvasDrawer() {
        canvas = new Canvas(100, 100);
    }

    protected void save() {
        new PpmBuilder(canvas).save("/tmp/test.ppm");
    }

    public void displayFrame() {
        RaytracerFrame frame = new RaytracerFrame(canvas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);
    }

    protected void addTupleToCanvas(Tuple tuple, Color color) {
        canvas.setPixel((int) tuple.x + 50, (int) tuple.y + 50, color);
    }

    public Canvas getCanvas() {
        return canvas;
    }
}
