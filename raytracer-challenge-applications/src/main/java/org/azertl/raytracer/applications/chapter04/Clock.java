package org.azertl.raytracer.applications.chapter04;

import org.azertl.raytracer.applications.AbstractCanvasDrawer;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;

public class Clock extends AbstractCanvasDrawer {

    public void generate() {
        Transformation transformation = new Transformation().translation(0, 40, 0);
        Tuple t;
        Color white = new Color(1, 1, 1);

        for (int i = 0; i < 12; i++) {
            t = transformation.rotationZ(Math.PI / 6).apply(Tuple.point(0, 0, 0));
            System.out.println(t);
            addTupleToCanvas(t, white);
        }
    }


    public static void main(String[] args) {
        Clock clock = new Clock();
        clock.generate();
        clock.save();
    }
}
