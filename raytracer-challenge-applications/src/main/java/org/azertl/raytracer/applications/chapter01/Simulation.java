package org.azertl.raytracer.applications.chapter01;

import org.azertl.raytracer.model.Canvas;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.utils.PpmBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Simulation {
    private static final Logger LOG = LoggerFactory.getLogger(Simulation.class);
    public static final Projectile PROJECTILE = new Projectile(
            Tuple.point(0, 1, 0),
            Tuple.vector(1, 1.8, 0).normalize().multiply(11.25)
    );
    public static final Environment ENVIRONMENT = new Environment(
            Tuple.vector(0, -0.1, 0),
            Tuple.vector(-0.01, 0, 0)
    );
    private final Environment environment;
    protected final Projectile projectile;

    private final List<Tuple> trajectory;

    public Simulation(Environment environment, Projectile projectile) {
        this.environment = environment;
        this.projectile = projectile;
        this.trajectory = new ArrayList<>();
    }

    private void tick() {
        projectile.setPosition(projectile.getPosition().add(projectile.getVelocity()));
        projectile.setVelocity(
                projectile.getVelocity()
                        .add(environment.getGravity())
                        .add(environment.getWind()));
    }

    public void run() {
        LOG.info("Start");
        updateTrajectory();
        while (projectile.getPosition().y >= 0) {
            tick();

            updateTrajectory();
        }
        LOG.info("End");

        saveTrajectory();
    }

    private void saveTrajectory() {

        double maxX = Double.MIN_VALUE;
        double maxY = Double.MIN_VALUE;

        for (Tuple tuple : trajectory) {
            if (tuple.x > maxX) {
                maxX = tuple.x;
            }
            if (tuple.y > maxY) {
                maxY = tuple.y;
            }
        }

        int canvasWidth = (int) Math.ceil(maxX) + 1;
        int canvasHeight = (int) Math.ceil(maxY) + 1;
        Canvas canvas = new Canvas(canvasWidth, canvasHeight);
        Color red = new Color(1, 0, 0);

        trajectory.forEach(tuple -> addTupleToCanvas(tuple, canvas, canvasHeight, red));

        new PpmBuilder(canvas).save("/tmp/test.ppm");
    }

    private static void addTupleToCanvas(Tuple tuple, Canvas canvas, int canvasHeight, Color red) {
        if (tuple.x > 0 && tuple.y > 0) {
            canvas.setPixel(
                    (int) Math.round(tuple.x),
                    canvasHeight - (int) Math.round(tuple.y),
                    red);
        }
    }

    private void updateTrajectory() {
        LOG.info(projectile.toString());
        trajectory.add(projectile.getPosition());
    }

    public static void main(String[] args) {
        new Simulation(ENVIRONMENT, PROJECTILE).run();
    }
}
