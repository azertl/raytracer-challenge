package org.azertl.raytracer.applications.chapter07;

import org.azertl.raytracer.applications.AbstractWorldDrawer;
import org.azertl.raytracer.model.Camera;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Material;
import org.azertl.raytracer.model.PointLight;
import org.azertl.raytracer.model.Sphere;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.operations.Transformations;

public class SceneDrawer extends AbstractWorldDrawer {

    public SceneDrawer(boolean allowShadows) {
        super(allowShadows);
    }

    protected Camera initCamera() {
        Camera camera = new Camera(400, 200, Math.PI / 3);
        camera.setTransform(Transformations.viewTransform(
                Tuple.point(0, 1.5, -5), Tuple.point(0, 1, 0), Tuple.vector(0, 1, 0)));
        return camera;
    }

    protected void populateWorld() {
        // floor : extremely flattened sphere with a matte structure
        Sphere floor = getWorld().createSphere();
        floor.setTransform(Transformations.scaling(10, 0.01, 10));
        Material floorMaterial = floor.getMaterial();
        floorMaterial.setColor(1, 0.9, 0.9);
        floorMaterial.setSpecular(0);

        // left wall : same scale as floor, rotated and translate
        Sphere leftWall = getWorld().createSphere();
        leftWall.setTransform(new Transformation()
                .scaling(10, 0.01, 10)
                .rotationX(Math.PI / 2)
                .rotationY(-Math.PI / 4)
                .translation(0, 0, 5));
        leftWall.setMaterial(floor.getMaterial());

        // right wall : identical to left wall, but rotated the opposite direction in y
        Sphere rightWall = getWorld().createSphere();
        rightWall.setTransform(new Transformation()
                .scaling(10, 0.01, 10)
                .rotationX(Math.PI / 2)
                .rotationY(Math.PI / 4)
                .translation(0, 0, 5));
        rightWall.setMaterial(floor.getMaterial());

        // large sphere in the middle is a unit sphere, translated upward, slightly and colored green
        Sphere middle = getWorld().createSphere();
        middle.setTransform(new Transformation().translation(-0.5, 1, 0.5));
        Material middleMaterial = middle.getMaterial();
        middleMaterial.setColor(0.1, 1, 0.5);
        middleMaterial.setDiffuse(0.7);
        middleMaterial.setSpecular(0.3);

        // smallest sphere is scaled by a third, before being translated
        Sphere left = getWorld().createSphere();
        left.setTransform(new Transformation().scaling(0.33, 0.33, 0.33).translation(-1.5, 0.33, -0.75));
        Material leftMaterial = left.getMaterial();
        leftMaterial.setColor(1, 0.8, 0.1);
        leftMaterial.setDiffuse(0.7);
        leftMaterial.setSpecular(0.3);

        // light source is white, shining from above and to the left
        PointLight lightSource = new PointLight(Tuple.point(-10, 10, -10), new Color(1, 1, 1));
        getWorld().setLightSource(lightSource);
    }

    public static void main(String[] args) {
        SceneDrawer sphereDrawer = new SceneDrawer(false);
        sphereDrawer.generate();
        sphereDrawer.displayFrame();
    }
}
