package org.azertl.raytracer.applications.chapter01;

import org.azertl.raytracer.model.Tuple;

public class Environment {
    private Tuple gravity;
    private Tuple wind;

    public Environment(Tuple gravity, Tuple wind) {
        if (!gravity.isVector()) {
            throw new IllegalArgumentException(gravity + " is not a vector.");
        }
        if (!wind.isVector()) {
            throw new IllegalArgumentException(wind + " is not a vector.");
        }
        this.gravity = gravity;
        this.wind = wind;
    }

    public Tuple getGravity() {
        return gravity;
    }

    public Tuple getWind() {
        return wind;
    }
}
