package org.azertl.raytracer.applications.chapter01;

import org.azertl.raytracer.model.Tuple;

public class Projectile {
    private Tuple position;
    private Tuple velocity;

    public Projectile(Tuple position, Tuple velocity) {
        setPosition(position);
        setVelocity(velocity);
    }

    public Tuple getPosition() {
        return position;
    }

    public void setPosition(Tuple position) {
        if (!position.isPoint()) {
            throw new IllegalArgumentException(position + " is not a point.");
        }
        this.position = position;
    }

    public Tuple getVelocity() {
        return velocity;
    }

    public void setVelocity(Tuple velocity) {
        if (!velocity.isVector()) {
            throw new IllegalArgumentException(velocity + " is not a vector.");
        }
        this.velocity = velocity;
    }

    @Override
    public String toString() {
        return String.format(
                "Projectile { pos = (%.2f, %.2f, %.2f), vel = [%.2f, %.2f, %.2f] }",
                position.x, position.y, position.z,
                velocity.x, velocity.y, velocity.z
        );
    }
}
