package org.azertl.raytracer;

import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Matrix;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.operations.Transformations;
import org.junit.jupiter.api.Test;

import javax.xml.crypto.dsig.Transform;

public class FluentTransformationsTest {
    @Test
    public void checkFluency() {
        Matrix expectedTransformationMatrix = Transformations.rotationX(Math.PI / 2);
        Transformation fluentTransformationMatrix = new Transformation()
                .rotationX(Math.PI / 2);
        Assertions.assertThat(fluentTransformationMatrix).isEqualTo(expectedTransformationMatrix);

        expectedTransformationMatrix =  Transformations.scaling(5, 5, 5).multiply(expectedTransformationMatrix);
        fluentTransformationMatrix.scaling(5,5,5);
        Assertions.assertThat(fluentTransformationMatrix).isEqualTo(expectedTransformationMatrix);

        expectedTransformationMatrix =  Transformations.translation(10, 5, 7).multiply(expectedTransformationMatrix);
        fluentTransformationMatrix.translation(10, 5, 7);
        Assertions.assertThat(fluentTransformationMatrix).isEqualTo(expectedTransformationMatrix);

        expectedTransformationMatrix = Transformations.rotationY(Math.PI / 4).multiply(Transformations.translation(0, -2, 5));
        fluentTransformationMatrix = new Transformation().translation(0,-2,5).rotationY(Math.PI / 4);
        Assertions.assertThat(fluentTransformationMatrix).isEqualTo(expectedTransformationMatrix);
    }
}
