package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.PointLight;
import org.azertl.raytracer.model.Sphere;
import org.azertl.raytracer.model.Tuple;

public class LightsDefinitions {
    @When("{variable} <- point_light\\({variable}, {variable})")
    public void define_point_light(String varLight, String varPoint, String varColor) {
        CommonDefinitions.put(
                varLight,
                new PointLight(
                        CommonDefinitions.getTuple(varPoint),
                        (Color) CommonDefinitions.getTuple(varColor)
                )
        );
    }

    @Given("{variable} <- point_light\\(point\\({int}, {int}, {int}), color\\({int}, {int}, {int}))")
    public void define_point_light(String varLight, int px, int py, int pz, int cr, int cg, int cb) {
        CommonDefinitions.put(
                varLight,
                new PointLight(
                        Tuple.point(px, py, pz),
                        new Color(cr, cg, cb)
                )
        );
    }

    @Then("{variable}.position = {variable}")
    public void assert_point_light_position_equals_variable(String varLight, String varPosition) {
        Assertions.assertThat(CommonDefinitions.getPointLight(varLight).getPosition())
                .isEqualTo(CommonDefinitions.getTuple(varPosition));
    }

    @And("{variable}.intensity = {variable}")
    public void assert_point_light_intensity_equals_variable(String varLight, String varColor) {
        Assertions.assertThat(CommonDefinitions.getPointLight(varLight).getIntensity())
                .isEqualTo(CommonDefinitions.getTuple(varColor));
    }

    @When("{variable} <- lighting\\({variable}, {variable}, {variable}, {variable}, {variable})")
    public void define_color_as_lighting(String varColor, String varMaterial, String varPointLight,
                                         String varPos, String varEyev, String varNormalv) {
        CommonDefinitions.put(
                varColor,
                CommonDefinitions.getMaterial(varMaterial)
                        .lighting(
                                CommonDefinitions.getPointLight(varPointLight),
                                new Sphere(), CommonDefinitions.getTuple(varPos),
                                CommonDefinitions.getTuple(varEyev),
                                CommonDefinitions.getTuple(varNormalv),
                                false)
        );
    }

    @Then("{variable} = color\\({double}, {double}, {double})")
    public void assert_color_equals_color(String varColor, double red, double green, double blue) {
        Assertions.assertThat(
                CommonDefinitions.getTuple(varColor)
        ).isEqualTo(
                new Color(red, green, blue)
        );
    }

    @When("{variable} <- lighting\\({variable}, {variable}, {variable}, {variable}, {variable}, {variable})")
    public void define_color_as_lighting(String varColor, String varMaterial, String varPointLight,
                                         String varPos, String varEyev, String varNormalv, String varInShadow) {
        CommonDefinitions.put(
                varColor,
                CommonDefinitions.getMaterial(varMaterial)
                        .lighting(
                                CommonDefinitions.getPointLight(varPointLight),
                                new Sphere(), CommonDefinitions.getTuple(varPos),
                                CommonDefinitions.getTuple(varEyev),
                                CommonDefinitions.getTuple(varNormalv),
                                CommonDefinitions.getBoolean(varInShadow)
                        )
        );
    }

    @When("{variable} <- lighting\\({variable}, {variable}, point\\({double}, {double}, {double}), {variable}, {variable}, false)")
    public void define_color_as_lighting(String varColor, String varMaterial, String varPointLight, double x, double y,
                                         double z, String varEyev, String varNormalv) {
        CommonDefinitions.put(
                varColor,
                CommonDefinitions.getMaterial(varMaterial)
                        .lighting(
                                CommonDefinitions.getPointLight(varPointLight),
                                new Sphere(), Tuple.point(x, y, z),
                                CommonDefinitions.getTuple(varEyev),
                                CommonDefinitions.getTuple(varNormalv),
                                false
                        )
        );
    }
}
