package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Camera;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.operations.Transformations;
import org.azertl.raytracer.utils.NumberUtils;

public class CameraDefinitions {

    @Given("{variable} <- {double}")
    public void define_variable_as_double_value(String variable, double value) {
        CommonDefinitions.put(variable, value);
    }

    @And("{variable} <- Pi\\/{int}")
    public void define_variable_as_fraction_of_pi(String variable, int denominator) {
        CommonDefinitions.put(variable, Math.PI / denominator);
    }

    @When("{variable} <- camera\\({variable}, {variable}, {variable})")
    public void define_camera(String varCamera, String varHsize, String varVsize, String varFieldOfView) {
        CommonDefinitions.put(
                varCamera,
                new Camera(
                        (int) CommonDefinitions.getDouble(varHsize),
                        (int) CommonDefinitions.getDouble(varVsize),
                        CommonDefinitions.getDouble(varFieldOfView)
                )
        );
    }

    @Then("{variable}.hsize = {int}")
    public void assert_camera_hsize_equals_value(String varCamera, int value) {
        Assertions.assertThat(
                CommonDefinitions.getCamera(varCamera).getHsize()
        ).isEqualTo(value);
    }

    @Then("{variable}.vsize = {int}")
    public void assert_camera_vsize_equals_value(String varCamera, int value) {
        Assertions.assertThat(
                CommonDefinitions.getCamera(varCamera).getVsize()
        ).isEqualTo(value);
    }

    @And("{variable}.field_of_view = Pi\\/{int}")
    public void assert_camera_vsize_equals_fraction_of_pi(String varCamera, int denominator) {
        Assertions.assertThat(
                CommonDefinitions.getCamera(varCamera).getFieldOfView()
        ).isEqualTo(Math.PI / denominator);
    }

    @Given("{variable} <- camera\\({int}, {int}, Pi\\/{int})")
    public void define_camera(String varCamera, int hsize, int vsize, int denominator) {
        CommonDefinitions.put(
                varCamera,
                new Camera(hsize, vsize, Math.PI / denominator)
        );
    }

    @Then("{variable}.pixel_size = {double}")
    public void assert_camera_pixelsize_equals_value(String varCamera, double value) {
        Assertions.assertThat(
                        NumberUtils.equalsDouble(CommonDefinitions.getCamera(varCamera).getPixelSize(), value))
                .isTrue();
    }

    @When("{variable} <- ray_for_pixel\\({variable}, {int}, {int})")
    public void define_ray_for_pixel(String varRay, String varCamera, int x, int y) {
        CommonDefinitions.put(
                varRay,
                CommonDefinitions.getCamera(varCamera).getRayForPixel(x, y)
        );
    }

    @When("{variable}.transform <- rotation_y\\(Pi\\/{int}) * translation\\({int}, {int}, {int})")
    public void define_camera_transform(String varCamera, int rotDen, int tx, int ty, int tz) {
        CommonDefinitions.getCamera(varCamera).setTransform(
                new Transformation().translation(tx, ty, tz).rotationY(Math.PI / rotDen)
        );
    }

    @When("{variable} <- render\\({variable}, {variable})")
    public void define_image_as_render_of_world_by_camera(String varCanvas, String varCamera, String varWorld) {
        CommonDefinitions.put(
                varCanvas,
                CommonDefinitions.getCamera(varCamera).render(CommonDefinitions.getWorld(varWorld))
        );
    }

    @And("{variable}.transform <- view_transform\\({variable}, {variable}, {variable})")
    public void set_camera_transform_as_view_transform(String varCamera, String varFrom, String varTo, String varUp) {
        CommonDefinitions.getCamera(varCamera).setTransform(Transformations.viewTransform(
                CommonDefinitions.getTuple(varFrom),
                CommonDefinitions.getTuple(varTo),
                CommonDefinitions.getTuple(varUp)
        ));
    }

}
