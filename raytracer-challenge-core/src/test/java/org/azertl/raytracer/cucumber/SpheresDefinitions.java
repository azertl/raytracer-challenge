package org.azertl.raytracer.cucumber;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Material;
import org.azertl.raytracer.model.Sphere;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.operations.Transformations;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpheresDefinitions {

    @When("{variable} <- normal_at\\({variable}, point\\(Sqrt\\({int})\\/{int}, Sqrt\\({int})\\/{int}, Sqrt\\({int})\\/{int}))")
    public void define_normal_at_point_on_sphere(String varVector, String varSphere, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5) {
        CommonDefinitions.put(
                varVector,
                CommonDefinitions.getSphere(varSphere).getNormalAt(Tuple.point(Math.sqrt(arg0) / arg1, Math.sqrt(arg2) / arg3, Math.sqrt(arg4) / arg5))
        );
    }

    @Then("{variable} = vector\\(Sqrt\\({int})\\/{int}, Sqrt\\({int})\\/{int}, Sqrt\\({int})\\/{int})")
    public void assert_vector_are_equal(String varVector, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5) {
        Assertions.assertThat(
                CommonDefinitions.getTuple(varVector)
        ).isEqualTo(
                Tuple.vector(Math.sqrt(arg0) / arg1, Math.sqrt(arg2) / arg3, Math.sqrt(arg4) / arg5)
        );
    }

    @Then("{variable} = vector\\({double}, {double}, {double})")
    public void assert_vector_are_equal(String varVector, double vx, double vy, double vz) {
        Assertions.assertThat(
                CommonDefinitions.getTuple(varVector)
        ).isEqualTo(
                Tuple.vector(vx, vy, vz)
        );
    }

    @Then("{variable} = normalize\\({variable})")
    public void assert_vector_equals_normalize_vector(String varVector1, String varVector2) {
        Assertions.assertThat(
                CommonDefinitions.getTuple(varVector1)
        ).isEqualTo(
                CommonDefinitions.getTuple(varVector2).normalize()
        );
    }

    @And("{variable} <- scaling\\({double}, {double}, {double}) * rotation_z\\(Pi\\/{int})")
    public void define_transformation(String varMatrix, double sx, double sy, double sz, int den) {
        CommonDefinitions.put(varMatrix,
                Transformations.scaling(sx, sy, sz).multiply(Transformations.rotationZ(Math.PI / den)));
    }

    @And("{variable} <- sphere\\() with :")
    public void define_sphere_with_datatable(String varSphere, DataTable datatable) {
        Sphere sphere = new Sphere();

        Map<String, String> map = datatable.asMap();

        map.forEach((key, value) -> {
            switch (key) {
                case "material.color":
                    setMaterialColor(value, sphere);
                    break;
                case "material.diffuse":
                    setMaterialDiffuse(value, sphere);
                    break;
                case "material.specular":
                    setMaterialSpecular(value, sphere);
                    break;
                case "transform":
                    transform(sphere, value);
                    break;
                default:
                    Assertions.fail("Unrecognized " + key);
            }
        });

        CommonDefinitions.put(varSphere, sphere);
    }

    private static void transform(Sphere sphere, String transformValue) {
        Pattern pattern = Pattern.compile("scaling\\((\\d+\\.\\d+), (\\d+\\.\\d+), (\\d+\\.\\d+)\\)");
        Matcher matcher = pattern.matcher(transformValue);

        if(matcher.find()) {
            Transformation transformation = new Transformation().scaling(
                    Double.parseDouble(matcher.group(1)),
                    Double.parseDouble(matcher.group(2)),
                    Double.parseDouble(matcher.group(3))
            );
            sphere.setTransform(transformation);
        }
    }

    private static void setMaterialSpecular(String value, Sphere sphere) {
        Material material = sphere.getMaterial();
        material.setSpecular(Double.parseDouble(value));
        sphere.setMaterial(material);
    }

    private static void setMaterialDiffuse(String value, Sphere sphere) {
        Material material = sphere.getMaterial();
        material.setDiffuse(Double.parseDouble(value));
        sphere.setMaterial(material);
    }

    private static void setMaterialColor(String value, Sphere sphere) {
        Material material = sphere.getMaterial();
        Pattern pattern = Pattern.compile("\\((\\d+\\.\\d+), (\\d+\\.\\d+), (\\d+\\.\\d+)\\)");
        Matcher matcher = pattern.matcher(value);
        if(matcher.find()) {
            String red = matcher.group(1);
            String green = matcher.group(2);
            String blue = matcher.group(3);
            material.setColor(Double.parseDouble(red), Double.parseDouble(green), Double.parseDouble(blue));
            sphere.setMaterial(material);
        } else {
            throw new IllegalArgumentException("Matcher did not find anything...");
        }
    }

    @And("{variable}.material.ambient <- {int}")
    public void set_material_ambient(String varSphere, int value) {
        CommonDefinitions.getSphere(varSphere).getMaterial().setAmbient(value);
    }

    @Then("{variable} = {variable}.material.color")
    public void assert_color_equals_sphere_material_color(String varColor, String varSphere) {
        Assertions.assertThat(CommonDefinitions.getColor(varColor))
                .isEqualTo(CommonDefinitions.getSphere(varSphere).getMaterial().getColor());
    }
}
