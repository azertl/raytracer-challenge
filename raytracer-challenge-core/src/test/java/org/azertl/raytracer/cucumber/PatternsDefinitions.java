package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.mock.TestPattern;
import org.azertl.raytracer.model.patterns.CheckersPattern;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.patterns.GradientPattern;
import org.azertl.raytracer.model.patterns.RingPattern;
import org.azertl.raytracer.model.patterns.StripePattern;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;

public class PatternsDefinitions {

    @Given("{variable} <- stripe_pattern\\({variable}, {variable})")
    public void define_stripe_pattern(String varPattern, String varColor1, String varColor2) {
        CommonDefinitions.put(
                varPattern,
                new StripePattern(CommonDefinitions.getColor(varColor1), CommonDefinitions.getColor(varColor2))
        );
    }

    @Then("{variable}.a = {variable}")
    public void assert_pattern_a_equals_color(String varPattern, String varColor) {
        Assertions.assertThat(((StripePattern) CommonDefinitions.getPattern(varPattern)).getColorA())
                .isEqualTo(CommonDefinitions.getColor(varColor));
    }

    @Then("{variable}.b = {variable}")
    public void assert_pattern_b_equals_color(String varPattern, String varColor) {
        Assertions.assertThat(((StripePattern) CommonDefinitions.getPattern(varPattern)).getColorB())
                .isEqualTo(CommonDefinitions.getColor(varColor));
    }

    @Then("stripe_at\\({variable}, point\\({double}, {double}, {double})) = {variable}")
    public void assert_stripe_at_pattern_and_point_equals_color(String varPattern, double x, double y, double z, String varColor) {
        Assertions.assertThat(
                ((StripePattern) CommonDefinitions.getPattern(varPattern)).getStripeAt(Tuple.point(x, y, z))
        ).isEqualTo(CommonDefinitions.getColor(varColor));
    }

    @When("{variable} <- stripe_at_object\\({variable}, {variable}, point\\({double}, {double}, {double}))")
    public void define_color_as_stripe_at_object(String varColor, String varPattern, String varShape, double x, double y, double z) {
        CommonDefinitions.put(
                varColor,
                CommonDefinitions.getPattern(varPattern).getPatternAtShape(
                        CommonDefinitions.getShape(varShape),
                        Tuple.point(x, y, z)
                )
        );
    }

    @And("set_pattern_transform\\({variable}, scaling\\({int}, {int}, {int}))")
    public void set_pattern_transform_scaling(String varPattern, int x, int y, int z) {
        CommonDefinitions.getPattern(varPattern).setTransform(new Transformation().scaling(x, y, z));
    }

    @And("set_pattern_transform\\({variable}, translation\\({double}, {double}, {double}))")
    public void set_pattern_transform_translation(String varPattern, double x, double y, double z) {
        CommonDefinitions.getPattern(varPattern).setTransform(new Transformation().translation(x, y, z));
    }

    @Given("{variable} <- test_pattern\\()")
    public void define_test_pattern(String varPattern) {
        CommonDefinitions.put(varPattern, new TestPattern());
    }

    @When("{variable} <- pattern_at_shape\\({variable}, {variable}, point\\({double}, {double}, {double}))")
    public void define_pattern_at_shape(String varColor, String varPattern, String varShape, double x, double y, double z) {
        CommonDefinitions.put(varColor,
                CommonDefinitions.getPattern(varPattern)
                        .getPatternAtShape(CommonDefinitions.getShape(varShape), Tuple.point(x, y, z)));
    }

    @Given("{variable} <- gradient_pattern\\(white, black)")
    public void define_gradient_pattern_of_white_and_black(String varPattern) {
        CommonDefinitions.put(varPattern,
                new GradientPattern(new Color(1, 1, 1), new Color(0, 0, 0)));
    }

    @And("pattern_at\\({variable}, point\\({double}, {double}, {double})) = color\\({double}, {double}, {double})")
    public void assert_pattern_at_point_equals_color(String varPattern, double x, double y, double z, double red, double green, double blue) {
        Assertions.assertThat(
                CommonDefinitions.getPattern(varPattern).getPatternAtShape(null, Tuple.point(x, y, z))
        ).isEqualTo(new Color(red, green, blue));
    }

    @Then("pattern_at\\({variable}, point\\({double}, {double}, {double})) = white")
    public void assert_pattern_at_point_equals_white(String varPattern, double x, double y, double z) {
        assert_pattern_at_point_equals_color(varPattern, x, y, z, 1.0, 1.0, 1.0);
    }

    @Then("pattern_at\\({variable}, point\\({double}, {double}, {double})) = black")
    public void assert_pattern_at_point_equals_black(String varPattern, double x, double y, double z) {
        assert_pattern_at_point_equals_color(varPattern, x, y, z, 0, 0, 0);
    }

    @Given("{variable} <- ring_pattern\\(white, black)")
    public void define_ring_pattern(String varPattern) {
        CommonDefinitions.put(varPattern, new RingPattern(new Color(1, 1, 1),
                new Color(0, 0, 0)));
    }


    @Given("{variable} <- checkers_pattern\\(white, black)")
    public void define_checkers_pattern(String varPattern) {
        CommonDefinitions.put(varPattern, new CheckersPattern(new Color(1, 1, 1),
                new Color(0, 0, 0)));
    }
}
