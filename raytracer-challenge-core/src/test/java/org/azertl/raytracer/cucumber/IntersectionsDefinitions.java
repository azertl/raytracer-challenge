package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.PreparedComputations;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.utils.NumberUtils;

public class IntersectionsDefinitions {
    @When("{variable} <- prepare_computations\\({variable}, {variable})")
    public void prepare_computations_intersection_shape(String varComputations, String varIntersection, String varRay) {
        CommonDefinitions.put(
                varComputations,
                PreparedComputations.build(CommonDefinitions.getIntersection(varIntersection), CommonDefinitions.getRay(varRay))
        );
    }

    @Then("{variable}.t = {variable}.t")
    public void assert_computations_time_equals_intersection_time(String varComputations, String varIntersection) {
        Assertions.assertThat(
                CommonDefinitions.getPreparedComputations(varComputations).getTime()
        ).isEqualTo(
                CommonDefinitions.getIntersection(varIntersection).getTime()
        );
    }

    @And("{variable}.object = {variable}.object")
    public void assert_computations_object_equals_intersection_object(String varComputations, String varIntersection) {
        Assertions.assertThat(
                CommonDefinitions.getPreparedComputations(varComputations).getObject()
        ).isEqualTo(
                CommonDefinitions.getIntersection(varIntersection).getObject()
        );
    }

    @And("{variable}.point = point\\({int}, {int}, {int})")
    public void assert_computations_point_equals_point(String varComputations, int px, int py, int pz) {
        Assertions.assertThat(
                CommonDefinitions.getPreparedComputations(varComputations).getPoint()
        ).isEqualTo(
                Tuple.point(px, py, pz)
        );
    }

    @And("{variable}.eyev = vector\\({int}, {int}, {int})")
    public void assert_computations_eyev_equals_vector(String varComputations, int vx, int vy, int vz) {
        Assertions.assertThat(
                CommonDefinitions.getPreparedComputations(varComputations).getEyev()
        ).isEqualTo(
                Tuple.point(vx, vy, vz)
        );
    }

    @And("{variable}.normalv = vector\\({int}, {int}, {int})")
    public void assert_computations_normalv_equals_vector(String varComputations, int vx, int vy, int vz) {
        Assertions.assertThat(
                CommonDefinitions.getPreparedComputations(varComputations).getNormalv()
        ).isEqualTo(
                Tuple.vector(vx, vy, vz)
        );
    }

    @Then("{variable}.inside = false")
    public void assert_computations_inside_equals_false(String varComputations) {
        Assertions.assertThat(
                CommonDefinitions.getPreparedComputations(varComputations).isInside()
        ).isFalse();
    }

    @And("{variable}.inside = true")
    public void assert_computations_inside_equals_true(String varComputations) {
        Assertions.assertThat(
                CommonDefinitions.getPreparedComputations(varComputations).isInside()
        ).isTrue();
    }

    @Then("{variable}.over_point.z < -EPSILON\\/2")
    public void assert_comps_overpoint_z_is_less_than_epsilon_over_2(String varComputations) {
        Assertions.assertThat(
                CommonDefinitions.getPreparedComputations(varComputations)
                        .getOverPoint().z
        ).isLessThan(-NumberUtils.EPSILON / 2);
    }

    @And("{variable}.point.z > {variable}.over_point.z")
    public void assert_comps_point_z_is_greater_than_comps_overpoint_z(String varComputations, String varComputations2) {
        PreparedComputations comps = CommonDefinitions.getPreparedComputations(varComputations);
        Assertions.assertThat(comps.getPoint().z).isGreaterThan(comps.getOverPoint().z);
    }
}
