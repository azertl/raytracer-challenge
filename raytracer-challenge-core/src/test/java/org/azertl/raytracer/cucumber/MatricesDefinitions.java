package org.azertl.raytracer.cucumber;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.operations.Matrices;
import org.azertl.raytracer.model.Matrix;
import org.azertl.raytracer.model.Tuple;

public class MatricesDefinitions {

    @Given("the following {int}x{int} matrix {variable} :")
    public void the_following_matrix_is(int rows, int columns, String variable, DataTable table) {
        CommonDefinitions.put(variable, readMatrix(table, rows, columns));
    }

    @Then("{variable}[{int},{int}] = {double}")
    public void mO(String variable, int row, int column, double expected) {
        Assertions.assertThat(CommonDefinitions.getMatrix(variable).get(row, column)).isEqualTo(expected);
    }

    @Given("the following matrix {variable} :")
    public void the_following_matrix_is(String variable, DataTable table) {
        int rows = table.height();
        int columns = table.width();

        CommonDefinitions.put(variable, readMatrix(table, rows, columns));
    }

    private static Matrix readMatrix(DataTable table) {
        return readMatrix(table, table.height(), table.width());
    }

    private static Matrix readMatrix(DataTable table, int rows, int columns) {
        Matrix m = new Matrix(rows, columns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                m.set(i, j, Double.parseDouble(table.cell(i, j)));
            }
        }

        return m;
    }

    @Then("{variable} = {variable}")
    public void objects_are_equal(String variable1, String variable2) {
        Assertions.assertThat(CommonDefinitions.get(variable1).equals(CommonDefinitions.get(variable2))).isTrue();
    }

    @Then("{variable} != {variable}")
    public void matrices_are_not_equal(String variable1, String variable2) {
        Assertions.assertThat(CommonDefinitions.getMatrix(variable1).equals(CommonDefinitions.getMatrix(variable2))).isFalse();
    }

    @Then("{variable} * {variable} is the following {int}x{int} matrix :")
    public void multiplication_of_matrices_is_the_following_matrix(String variable1, String variable2, int rows, int columns, DataTable table) {
        Matrix A = CommonDefinitions.getMatrix(variable1);
        Matrix B = CommonDefinitions.getMatrix(variable2);

        Matrix expected = readMatrix(table, rows, columns);

        Assertions.assertThat(A.multiply(B)).isEqualTo(expected);
    }

    @Then("{variable} * {variable} = tuple\\({double}, {double}, {double}, {double})")
    public void multiplication_of_matrix_by_tuple_is_tuple(String variable1, String variable2, double x, double y, double z, double w) {
        Matrix matrixA = CommonDefinitions.getMatrix(variable1);
        Tuple tupleB = CommonDefinitions.getTuple(variable2);

        Assertions.assertThat(matrixA.multiply(tupleB)).isEqualTo(new Tuple(x, y, z, w));
    }

    @Then("{variable} * identity_matrix = {variable}")
    public void multiplication_of_matrix_by_identity_matrix_is_matrix(String variable1, String variable2) {
        Assertions.assertThat(CommonDefinitions.getMatrix(variable1).multiply(Matrices.identityMatrix())).isEqualTo(CommonDefinitions.getMatrix(variable2));
    }

    @Then("transpose\\({variable}) is the following matrix :")
    public void transpose_of_matrix_is_the_following_matrix(String variable, DataTable table) {
        Matrix expected = readMatrix(table);

        Assertions.assertThat(CommonDefinitions.getMatrix(variable).transpose()).isEqualTo(expected);
    }

    @Given("{variable} <- transpose\\(identity_matrix)")
    public void matrix_is_transpose_identity_matrix(String variable) {
        CommonDefinitions.put(variable, Matrices.identityMatrix().transpose());
    }

    @Then("{variable} = identity_matrix")
    public void matrix_is_identity_matrix(String variable) {
        Assertions.assertThat(CommonDefinitions.getMatrix(variable)).isEqualTo(Matrices.identityMatrix());
    }

    @Then("determinant\\({variable}) = {double}")
    public void determinant_of_matrix_is(String variable, double expected) {
        Assertions.assertThat(Matrices.determinant(CommonDefinitions.getMatrix(variable))).isEqualTo(expected);
    }

    @Then("Submatrix\\({variable}, {int}, {int}) is the following {int}x{int} matrix :")
    public void submatrix_of_matrix_is(String variable, int row, int col, int rows, int cols, DataTable table) {
        Matrix submatrix = CommonDefinitions.getMatrix(variable).submatrix(row, col);

        Assertions.assertThat(submatrix.rows()).isEqualTo(rows);
        Assertions.assertThat(submatrix.cols()).isEqualTo(cols);

        Matrix expected = readMatrix(table);
        Assertions.assertThat(submatrix).isEqualTo(expected);
    }

    @Given("{variable} <- submatrix\\({variable}, {int}, {int})")
    public void matrix_is_submatrix_of_matrix(String variable1, String variable2, int row, int col) {
        CommonDefinitions.put(
                variable1,
                CommonDefinitions.getMatrix(variable2).submatrix(row, col)
        );
    }

    @Then("minor\\({variable}, {int}, {int}) = {double}")
    public void minor_of_matrix_equals_expected(String variable, int row, int col, double expected) {
        Assertions.assertThat(Matrices.minor(CommonDefinitions.getMatrix(variable), row, col))
                .isEqualTo(expected);
    }

    @Then("cofactor\\({variable}, {int}, {int}) = {double}")
    public void cofactor_of_matrix_equals_expected(String variable, int row, int col, double expected) {
        Assertions.assertThat(Matrices.cofactor(CommonDefinitions.getMatrix(variable), row, col)).isEqualTo(expected);
    }

    @Then("{variable} is invertible")
    public void matrix_is_invertible(String variable) {
        Assertions.assertThat(Matrices.isInvertible(CommonDefinitions.getMatrix(variable)))
                .isTrue();
    }

    @Then("{variable} is not invertible")
    public void matrix_is_not_invertible(String variable) {
        Assertions.assertThat(Matrices.isInvertible(CommonDefinitions.getMatrix(variable)))
                .isFalse();
    }

    @Then("{variable} <- inverse\\({variable})")
    public void matrix_is_inverse_of_matrix(String variable1, String variable2) {
        CommonDefinitions.put(variable1, CommonDefinitions.getMatrix(variable2).inverse());
    }

    @Then("{variable}[{int}, {int}] = {int}\\/{int}")
    public void matrix_cell_value_is_fraction(String variable, int row, int col, int num, int denom) {
        Assertions.assertThat(CommonDefinitions.getMatrix(variable).get(row, col)).isEqualTo(((double) num) / denom);

    }

    @And("{variable} is the following {int}x{int} matrix :")
    public void matrix_is_the_following_matrix(String variable, int rows, int cols, DataTable table) {
        Matrix matrix = CommonDefinitions.getMatrix(variable);
        Assertions.assertThat(matrix.rows()).isEqualTo(rows);
        Assertions.assertThat(matrix.cols()).isEqualTo(cols);

        Assertions.assertThat(matrix).isEqualTo(readMatrix(table));
    }

    @And("{variable} <- {double} * {variable}")
    public void matrix_is_scalar_multiplied_by_matrix(String variable1, double scalar, String variable2) {
        CommonDefinitions.put(variable1, CommonDefinitions.getMatrix(variable2).multiply(scalar));
    }

    @Then("inverse\\({variable}) is the following {int}x{int} matrix :")
    public void inverse_of_matrix_is_the_following_matrix(String variable, int rows, int cols, DataTable table) {
        Matrix inversed = CommonDefinitions.getMatrix(variable).inverse();
        Assertions.assertThat(inversed.rows()).isEqualTo(rows);
        Assertions.assertThat(inversed.cols()).isEqualTo(cols);
        Assertions
                .assertThat(inversed)
                .isEqualTo(readMatrix(table));
    }

    @And("{variable} <- {variable} * {variable}")
    public void matrix_is_multiplication_of_matrices(String variable1, String variable2, String variable3) {
        Object obj = CommonDefinitions.get(variable3);

        if (obj instanceof Tuple t) {
            CommonDefinitions.put(variable1, CommonDefinitions.getMatrix(variable2).multiply(t));
        } else if (obj instanceof Matrix m) {
            CommonDefinitions.put(variable1, CommonDefinitions.getMatrix(variable2).multiply(m));
        }
    }

    @Then("{variable} * inverse\\({variable}) = {variable}")
    public void matrix_multiplied_by_inverse_of_matrix_equals_matrix(String variable1, String variable2, String variable3) {
        Assertions.assertThat(
                        CommonDefinitions.getMatrix(variable1).multiply(
                                CommonDefinitions.getMatrix(variable2).inverse()))
                .isEqualTo(CommonDefinitions.getMatrix(variable3));
    }

    @Given("{variable} <- identity_matrix")
    public void define_identity_matrix(String variable) {
        CommonDefinitions.put(variable, Matrices.identityMatrix());
    }

    @Then("{variable} * {variable} = identity_matrix")
    public void multiplication_of_matrices_equals_identity_matrix(String variable1, String variable2) {
        Assertions.assertThat(
                CommonDefinitions.getMatrix(variable1).multiply(
                        CommonDefinitions.getMatrix(variable2)
                )
        ).isEqualTo(Matrices.identityMatrix());
    }

    @And("{variable} <- transpose\\({variable})")
    public void define_matrix_as_transpose_of_matrix(String variable1, String variable2) {
        CommonDefinitions.put(variable1,
                CommonDefinitions.getMatrix(variable2).transpose(
                ));
    }

    @Then("{variable} * {variable} = {variable}")
    public void matrix_multiplied_by_tuple_equals_matrix(String variable1, String variable2, String variable3) {
        Assertions.assertThat(
                CommonDefinitions.getMatrix(variable1).multiply(
                        CommonDefinitions.getTuple(variable2)
                )
        ).isEqualTo(CommonDefinitions.getTuple(variable3));
    }

    @Then("{variable} is the following 4x4 matrix:")
    public void assert_matrix_equals_matrix(String varTransform, DataTable table) {
        Assertions.assertThat(CommonDefinitions.getMatrix(varTransform))
                .isEqualTo(readMatrix(table));
    }
}
