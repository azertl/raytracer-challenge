package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.operations.Tuples;

public class TuplesDefinitions {

    @Given("{variable} <- tuple\\({double}, {double}, {double}, {double})")
    public void define_tuple(String variable, Double double1, Double double2, Double double3, Double double4) {
        CommonDefinitions.put(variable, new Tuple(double1, double2, double3, double4));
    }

    @Given("{variable} <- point\\({double}, {double}, {double})")
    public void define_point(String variable, Double double1, Double double2, Double double3) {
        CommonDefinitions.put(variable, Tuple.point(double1, double2, double3));
    }

    @Given("{variable} <- vector\\({double}, {double}, {double})")
    public void define_vector(String variable, Double double1, Double double2, Double double3) {
        CommonDefinitions.put(variable, Tuple.vector(double1, double2, double3));
    }

    @Given("{variable} <- color\\({double}, {double}, {double})")
    public void define_color(String variable, double double1, double double2, double double3) {
        CommonDefinitions.put(variable, new Color(double1, double2, double3));
    }

    @Then("{variable}.x = {double}")
    public void tuple_x_equals(String variable, double arg) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).x).isEqualTo(arg);
    }

    @Then("{variable}.y = {double}")
    public void tuple_y_equals(String variable, double arg) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).y).isEqualTo(arg);
    }

    @Then("{variable}.z = {double}")
    public void tuple_z_equals(String variable, double arg) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).z).isEqualTo(arg);
    }

    @Then("{variable}.w = {double}")
    public void tuple_w_equals(String variable, double arg) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).w).isEqualTo(arg);
    }

    @Then("{variable} is a point")
    public void tuple_is_a_point(String variable) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).isPoint()).isTrue();
    }

    @Then("{variable} is not a point")
    public void tuple_is_not_a_point(String variable) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).isPoint()).isFalse();
    }

    @Then("{variable} is a vector")
    public void tuple_is_a_vector(String variable) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).isVector()).isTrue();
    }

    @Then("{variable} is not a vector")
    public void tuple_is_not_a_vector(String variable) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).isVector()).isFalse();
    }

    @Then("{variable} = tuple\\({double}, {double}, {double}, {double})")
    public void tuple_equals_tuple(String variable, Double double1, Double double2, Double double3, Double double4) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable)).isEqualTo(new Tuple(double1, double2, double3, double4));
    }

    @Then("{variable} + {variable} = tuple\\({double}, {double}, {double}, {double})")
    public void tuple_plus_tuple_equals_tuple(String variable1, String variable2, Double double1, Double double2, Double double3, Double double4) {
        Assertions.assertThat(addTuples(variable1, variable2)).isEqualTo(new Tuple(double1, double2, double3, double4));
    }

    @Then("{variable} + {variable} is not a point")
    public void tuple_plus_tuple_is_not_a_point(String variable1, String variable2) {
        Assertions.assertThat(addTuples(variable1, variable2).isPoint()).isFalse();
    }

    @Then("{variable} + {variable} is not a vector")
    public void tuple_plus_tuple_is_not_a_vector(String variable1, String variable2) {
        Assertions.assertThat(addTuples(variable1, variable2).isVector()).isFalse();
    }

    @Then("{variable} - {variable} = vector\\({double}, {double}, {double})")
    public void tuple_minus_tuple_equals_vector(String variable1, String variable2, Double double1, Double double2, Double double3) {
        Assertions.assertThat(minusTuples(variable1, variable2)).isEqualTo(Tuple.vector(double1, double2, double3));
    }

    @Then("{variable} - {variable} = point\\({double}, {double}, {double})")
    public void tuple_minus_tuple_equals_point(String variable1, String variable2, Double double1, Double double2, Double double3) {
        Assertions.assertThat(minusTuples(variable1, variable2)).isEqualTo(Tuple.point(double1, double2, double3));
    }

    @Then("{variable} - {variable} = tuple\\({double}, {double}, {double}, {double})")
    public void tuple_minus_tuple_equals_tuple(String variable1, String variable2, Double double1, Double double2, Double double3, Double double4) {
        Assertions.assertThat(minusTuples(variable1, variable2))
                .isEqualTo(new Tuple(double1, double2, double3, double4));
    }

    @Then("{variable} - {variable} is not a point")
    public void tuple_minus_tuple_is_not_a_point(String variable1, String variable2) {
        Assertions.assertThat(minusTuples(variable1, variable2).isPoint()).isFalse();
    }

    @Then("{variable} - {variable} is not a vector")
    public void tuple_minus_tuple_is_not_a_vector(String variable1, String variable2) {
        Assertions.assertThat(minusTuples(variable1, variable2).isVector()).isFalse();
    }

    @Then("-{variable} = tuple\\({double}, {double}, {double}, {double})")
    public void negate_tuple_equals_tuple(String variable, double double1, double double2, double double3, double double4) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).negate())
                .isEqualTo(new Tuple(double1, double2, double3, double4));
    }

    private Tuple addTuples(String variable1, String variable2) {
        return CommonDefinitions.getTuple(variable1)
                .add(CommonDefinitions.getTuple(variable2));
    }

    private Tuple minusTuples(String variable1, String variable2) {
        return CommonDefinitions.getTuple(variable1).subtract(
                CommonDefinitions.getTuple(variable2));
    }

    @Then("{variable} * {double} = tuple\\({double}, {double}, {double}, {double})")
    public void tuple_times_scalar_equals_tuple(String variable, double double1, double double2, double double3, double double4, double double5) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).multiply(double1))
                .isEqualTo(new Tuple(double2, double3, double4, double5));
    }

    @Then("{variable} \\/ {double} = tuple\\({double}, {double}, {double}, {double})")
    public void tuple_divided_by_scalar_equals_tuple(String variable, double double1, double double2, double double3, double double4, double double5) {
        Assertions.assertThat(Tuples.divide(CommonDefinitions.getTuple(variable), double1))
                .isEqualTo(new Tuple(double2, double3, double4, double5));
    }

    @Then("magnitude\\({variable}) = sqrt\\({double})")
    public void magnitude_of_vector_equals_value(String variable, double expected) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).magnitude())
                .isEqualTo(Math.sqrt(expected));
    }

    @Then("normalize\\({variable}) = vector\\({double}, {double}, {double})")
    public void normalizing_vector_equals_vector(String variable, double double1, double double2, double double3) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).normalize())
                .isEqualTo(Tuple.vector(double1, double2, double3));
    }

    @Then("normalize\\({variable}) = approximately vector\\({double}, {double}, {double})")
    public void normalizing_vector_equals_approximately_vector(String variable, double double1, double double2, double double3) {
        Assertions.assertThat(
                CommonDefinitions.getTuple(variable).normalize()
                        .approxEquals(
                                Tuple.vector(double1, double2, double3))
        ).isTrue();
    }

    @When("{variable} <- normalize\\({variable})")
    public void tuple_is_normalize_tuple(String variable1, String variable2) {
        CommonDefinitions.put(variable1, CommonDefinitions.getTuple(variable2).normalize());
    }

    @Then("magnitude\\({variable}) = {double}")
    public void magnitude_of_tuple_equals_value(String variable, double expected) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).magnitude()).isEqualTo(expected);
    }

    @Then("dot\\({variable}, {variable}) = {double}")
    public void dot_product_of_tuples_equals_value(String variable1, String variable2, double expected) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable1).dot(CommonDefinitions.getTuple(variable2))).isEqualTo(expected);
    }

    @Then("cross\\({variable}, {variable}) = vector\\({double}, {double}, {double})")
    public void cross_tuple_by_tuple_equals_vector(String variable1, String variable2, double double1, double double2, double double3) {
        Assertions.assertThat(Tuples.cross(CommonDefinitions.getTuple(variable1), CommonDefinitions.getTuple(variable2)))
                .isEqualTo(Tuple.vector(double1, double2, double3));
    }

    @Then("{variable}.red = {double}")
    public void color_red_equals_expected(String variable, double expected) {
        Assertions.assertThat(((Color) CommonDefinitions.getTuple(variable)).red())
                .isEqualTo(expected);
    }

    @Then("{variable}.green = {double}")
    public void color_green_equals_expected(String variable, double expected) {
        Assertions.assertThat(((Color) CommonDefinitions.getTuple(variable)).green())
                .isEqualTo(expected);
    }

    @Then("{variable}.blue = {double}")
    public void color_blue_equals_expected(String variable, double expected) {
        Assertions.assertThat(((Color) CommonDefinitions.getTuple(variable)).blue())
                .isEqualTo(expected);
    }

    @Then("{variable} + {variable} = color\\({double}, {double}, {double})")
    public void color_plus_color_equals_color(String variable1, String variable2, double double1, double double2, double double3) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable1).add(CommonDefinitions.getTuple(variable2)))
                .isEqualTo(new Color(double1, double2, double3));
    }

    @Then("{variable} - {variable} = color\\({double}, {double}, {double})")
    public void color_minus_color_equals_color(String variable1, String variable2, double double1, double double2, double double3) {
        Assertions.assertThat(
                CommonDefinitions.getTuple(variable1).subtract(CommonDefinitions.getTuple(variable2))
        ).isEqualTo(
                new Color(double1, double2, double3)
        );
    }

    @Then("{variable} * {double} = color\\({double}, {double}, {double})")
    public void color_times_scalar_equals_color(String variable, double double1, double double2, double double3, double double4) {
        Assertions.assertThat(CommonDefinitions.getTuple(variable).multiply(double1))
                .isEqualTo(new Color(double2, double3, double4));
    }

    @Then("{variable} * {variable} = color\\({double}, {double}, {double})")
    public void color_times_color_equals_color(String variable1, String variable2, double double1, double double2, double double3) {
        Assertions.assertThat(CommonDefinitions.getColor(variable1).hadamardProduct(CommonDefinitions.getTuple(variable2)))
                .isEqualTo(new Color(double1, double2, double3));
    }

    @Then("{variable} = {variable} * {double}")
    public void tuple_equals_scalar_times_tuple(String variable1, String variable2, double scalar) {
        Assertions.assertThat(
                CommonDefinitions.getTuple(variable1)
        ).isEqualTo(CommonDefinitions.getTuple(variable2).multiply(scalar));
    }

    @When("{variable} <- reflect\\({variable}, {variable})")
    public void define_reflect_of_vector_and_normal(String varReflect, String varVector, String varNormal) {
        CommonDefinitions.put(
                varReflect,
                CommonDefinitions.getTuple(varVector).reflect(CommonDefinitions.getTuple(varNormal))
        );
    }

    @And("{variable} <- vector\\(Sqrt\\({int})\\/{int}, Sqrt\\({int})\\/{int}, {int})")
    public void define_vector_z(String varVector, int nx, int dx, int ny, int dy, int z) {
        CommonDefinitions.put(
                varVector,
                Tuple.vector(Math.sqrt(nx) / dx, Math.sqrt(ny) / dy, z)
        );
    }

    @Given("{variable} <- vector\\({int}, Sqrt\\({int})\\/{int}, -Sqrt\\({int})\\/{int})")
    public void define_vector_x(String varVector, int x, int ny, int dy, int nz, int dz) {
        CommonDefinitions.put(
                varVector,
                Tuple.vector(x, Math.sqrt(ny) / dy, -Math.sqrt(nz) / dz)
        );
    }

    @Given("{variable} <- vector\\({int}, -Sqrt\\({int})\\/{int}, -Sqrt\\({int})\\/{int})")
    public void define_vector_x_minus_y(String varVector, int x, int ny, int dy, int nz, int dz) {
        CommonDefinitions.put(
                varVector,
                Tuple.vector(x, -Math.sqrt(ny) / dy, -Math.sqrt(nz) / dz)
        );
    }
}
