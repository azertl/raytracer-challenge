package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.PointLight;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.model.World;

public class WorldDefinitions {

    @Given("{variable} <- world\\()")
    public void define_new_world(String varWorld) {
        CommonDefinitions.put(varWorld, new World());
    }

    @Then("{variable} contains no objects")
    public void assert_world_contains_no_object(String varWorld) {
        Assertions.assertThat(
                CommonDefinitions.getWorld(varWorld).getObjects()
        ).isEmpty();
    }

    @And("{variable} has no light source")
    public void assert_world_has_no_light_source(String varWorld) {
        Assertions.assertThat(
                CommonDefinitions.getWorld(varWorld).getLightSource()
        ).isNull();
    }

    @When("{variable} <- default_world\\()")
    public void define_default_world(String varWorld) {
        CommonDefinitions.put(varWorld, World.createDefaultWorld());
    }

    @Then("{variable}.light = {variable}")
    public void assert_world_light_equals_variable(String varWorld, String varLight) {
        Assertions.assertThat(CommonDefinitions.getWorld(varWorld).getLightSource())
                .isEqualTo(CommonDefinitions.getPointLight(varLight));
    }

    @And("{variable} contains {variable}")
    public void assert_world_contains_object(String varWorld, String varObject) {
        Assertions.assertThat(CommonDefinitions.getWorld(varWorld).getObjects())
                .contains((CommonDefinitions.getSphere(varObject)));
    }

    @When("{variable} <- intersect_world\\({variable}, {variable})")
    public void define_intersect_world_by_ray(String varIntersections, String varWorld, String varRay) {
        CommonDefinitions.put(
                varIntersections,
                CommonDefinitions.getRay(varRay).intersect(CommonDefinitions.getWorld(varWorld))
        );
    }

    @And("{variable} <- the first object in {variable}")
    public void define_first_object_in_world(String varSphere, String varWorld) {
        CommonDefinitions.put(
                varSphere,
                CommonDefinitions.getWorld(varWorld).getObject(0)
        );
    }

    @And("{variable} <- the second object in {variable}")
    public void define_second_object_in_world(String varSphere, String varWorld) {
        CommonDefinitions.put(
                varSphere,
                CommonDefinitions.getWorld(varWorld).getObject(1)
        );
    }

    @And("{variable} <- shade_hit\\({variable}, {variable})")
    public void define_color_as_shade_hit_of_world_and_comps(String varColor, String varWorld, String varComputations) {
        CommonDefinitions.put(
                varColor,
                CommonDefinitions.getWorld(varWorld).shadeHit(CommonDefinitions.getPreparedComputations(varComputations))
        );
    }

    @And("{variable}.light <- point_light\\(point\\({int}, {double}, {int}), color\\({int}, {int}, {int}))")
    public void define_world_light(String varWorld, int px, double py, int pz, int red, int green, int blue) {
        CommonDefinitions.getWorld(varWorld).setLightSource(new PointLight(Tuple.point(px, py, pz), new Color(red, green, blue)));
    }

    @When("{variable} <- color_at\\({variable}, {variable})")
    public void define_color_at_world_and_ray(String varColor, String varWorld, String varRay) {
        CommonDefinitions.put(
                varColor,
                CommonDefinitions.getWorld(varWorld).colorAt(CommonDefinitions.getRay(varRay))
        );
    }

    @Then("isShadowed\\({variable}, {variable}) is false")
    public void assert_isShadowed_world_point_is_false(String varWorld, String varTuple) {
        Assertions.assertThat(CommonDefinitions.getWorld(varWorld).isShadowed(CommonDefinitions.getTuple(varTuple)))
                .isFalse();
    }

    @Then("isShadowed\\({variable}, {variable}) is true")
    public void assert_isShadowed_world_point_is_true(String varWorld, String varTuple) {
        Assertions.assertThat(CommonDefinitions.getWorld(varWorld).isShadowed(CommonDefinitions.getTuple(varTuple)))
                .isTrue();
    }

    @And("{variable} is added to {variable}")
    public void sphere_is_added_to_world(String varSphere, String varWorld) {
        CommonDefinitions.getWorld(varWorld).addObject(CommonDefinitions.getSphere(varSphere));
    }
}
