package org.azertl.raytracer.cucumber;

import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import org.azertl.raytracer.model.*;
import org.azertl.raytracer.model.patterns.Pattern;

import java.util.HashMap;
import java.util.Map;

public class CommonDefinitions {
    private static Map<String, Object> variables;

    public static Shape getShape(String name) {
        return (Shape) variables.get(name);
    }

    public static Sphere getSphere(String name) {
        return (Sphere) variables.get(name);
    }

    public static Intersection getIntersection(String name) {
        return (Intersection) variables.get(name);
    }

    public static Intersections getIntersections(String name) {
        return (Intersections) variables.get(name);
    }

    public static Material getMaterial(String name) {
        return (Material) variables.get(name);
    }

    public static PointLight getPointLight(String name) {
        return (PointLight) variables.get(name);
    }

    public static World getWorld(String name) {
        return (World) variables.get(name);
    }

    public static Canvas getCanvas(String name) {
        return (Canvas) variables.get(name);
    }

    public static PreparedComputations getPreparedComputations(String name) {
        return (PreparedComputations) variables.get(name);
    }

    public static double getDouble(String name) {
        return (Double) variables.get(name);
    }

    public static Camera getCamera(String name) {
        return (Camera) variables.get(name);
    }

    public static boolean getBoolean(String name) {
        return (Boolean) variables.get(name);
    }

    public static Plane getPlane(String varPlane) {
        return (Plane) variables.get(varPlane);
    }

    public static Pattern getPattern(String varPattern) {
        return (Pattern) variables.get(varPattern);
    }

    @Before
    public void beforeScenario() {
        variables= new HashMap<>();
    }

    public static void put(String name, Object obj) {
        variables.put(name, obj);
    }

    public static Tuple getTuple(String name) {
        return (Tuple) variables.get(name);
    }

    public static Color getColor(String name) {
        return (Color) variables.get(name);
    }

    public static Matrix getMatrix(String name) {
        return (Matrix) variables.get(name);
    }

    public static Ray getRay(String name) {
        return (Ray) variables.get(name);
    }

    public static Object get(String name) {
        return variables.get(name);
    }


    @ParameterType("[a-zA-Z][a-zA-Z0-9]*")
    public String variable(String variable) {
        return variable;
    }

}
