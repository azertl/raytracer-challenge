package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Canvas;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.utils.PpmBuilder;

import java.util.Scanner;

public class CanvasDefinitions {
    private Canvas canvas;
    private String ppm;

    @Given("c <- canvas\\({int}, {int})")
    public void define_canvas(int width, int height) {
        canvas = new Canvas(width, height);
    }

    @Then("c.width = {int}")
    public void canvas_width_equals_expected(int width) {
        Assertions.assertThat(canvas.width).isEqualTo(width);
    }

    @And("c.height = {int}")
    public void canvas_height_equals_expected(int height) {
        Assertions.assertThat(canvas.height).isEqualTo(height);
    }

    @And("every pixel of c is color\\({int}, {int}, {int})")
    public void every_pixel_of_canvas_is_color(int red, int green, int blue) {
        for (int i = 0; i < canvas.width; i++) {
            for (int j = 0; j < canvas.height; j++) {
                assertPixelColor(i, j, new Color(0, 0, 0));
            }
        }
    }

    @When("write_pixel\\(c, {int}, {int}, {variable})")
    public void write_pixel_color(int i, int j, String variable) {
        canvas.setPixel(i, j, CommonDefinitions.getTuple(variable));
    }

    @Then("pixel_at\\(c, {int}, {int}) = {variable}")
    public void pixel_at_position_equals_color(int i, int j, String variable) {
        assertPixelColor(i, j, CommonDefinitions.getTuple(variable));
    }

    private void assertPixelColor(int i, int j, Tuple variable) {
        Assertions.assertThat(canvas.getPixel(i, j)).isEqualTo(variable);
    }

    @When("ppm <- canvas_to_ppm\\(c)")
    public void define_ppm_as_canvas_to_ppm() {
        ppm = new PpmBuilder(canvas).build();
    }

    @Then("lines {int}-{int} of ppm are")
    public void lines_from_to_of_ppm_are_multiline(int beg, int end, String multi) {
        Scanner scanner = new Scanner(new PpmBuilder(canvas).build());
        StringBuilder sb = new StringBuilder();
        int lineNb = 0;
        boolean firstLine = true;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (lineNb >= beg - 1 && lineNb < end) {
                if (!firstLine) {
                    sb.append("\n");
                }
                sb.append(line);
                firstLine = false;
            }
            lineNb++;
        }

        Assertions.assertThat(sb.toString()).isEqualTo(multi);
    }

    @When("every pixel of c is set to color\\({double}, {double}, {double})")
    public void every_pixel_of_canvas_is_set_to_color(double red, double green, double blue) {
        for (int i = 0; i < canvas.width; i++) {
            for (int j = 0; j < canvas.height; j++) {
                canvas.setPixel(i, j, new Color(red, green, blue));
            }
        }
    }

    @Then("ppm ends with a newline character")
    public void ppm_ends_with_a_newline_character() {
        Assertions.assertThat(ppm).endsWith("\n");
    }

    @Then("pixel_at\\({variable}, {int}, {int}) = color\\({double}, {double}, {double})")
    public void assert_pixel_at_equals_color(String varCanvas, int x, int y, double red, double green, double blue) {
        Assertions.assertThat(
                CommonDefinitions.getCanvas(varCanvas).getPixel(x, y)
        ).isEqualTo(new Color(red, green, blue));
    }
}
