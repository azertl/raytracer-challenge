package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Plane;

public class PlanesDefinitions {

    @Given("{variable} <- plane\\()")
    public void define_plane(String varPlane) {
        CommonDefinitions.put(varPlane, new Plane());
    }

    @When("{variable} <- local_intersect\\({variable}, {variable})")
    public void define_local_intersections_of_shape_by_ray(String varIntersections, String varPlane, String varRay) {
        CommonDefinitions.put(
                varIntersections,
                CommonDefinitions.getPlane(varPlane).getLocalIntersections(CommonDefinitions.getRay(varRay))
        );
    }

    @Then("{variable} is empty")
    public void assert_intersections_is_empty(String varIntersections) {
        Assertions.assertThat(CommonDefinitions.getIntersections(varIntersections).isEmpty()).isTrue();
    }
}
