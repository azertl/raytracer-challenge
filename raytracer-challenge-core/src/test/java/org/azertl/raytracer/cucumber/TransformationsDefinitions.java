package org.azertl.raytracer.cucumber;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.operations.Transformations;

public class TransformationsDefinitions {

    @Given("{variable} <- translation\\({double}, {double}, {double})")
    public void define_translation(String variable, double double1, double double2, double double3) {
        CommonDefinitions.put(
                variable, Transformations.translation(double1, double2, double3)
        );
    }

    @Then("{variable} * {variable} = point\\({double}, {double}, {double})")
    public void matrix_multiplied_by_point_equals_point(String variable1, String variable2, double double1, double double2, double double3) {
        Assertions.assertThat(
                        CommonDefinitions.getMatrix(variable1).multiply(
                                CommonDefinitions.getTuple(variable2)
                        ))
                .isEqualTo(Tuple.point(double1, double2, double3));
    }

    @Given("{variable} <- scaling\\({double}, {double}, {double})")
    public void define_scaling(String variable, double double1, double double2, double double3) {
        CommonDefinitions.put(
                variable, Transformations.scaling(double1, double2, double3)
        );
    }

    @Then("{variable} * {variable} = vector\\({double}, {double}, {double})")
    public void matrix_multiplied_by_vector_equals_vector(String variable1, String variable2, double double1, double double2, double double3) {
        Assertions.assertThat(
                        CommonDefinitions.getMatrix(variable1).multiply(
                                CommonDefinitions.getTuple(variable2)
                        ))
                .isEqualTo(Tuple.vector(double1, double2, double3));
    }

    @Then("{variable} <- rotation_x\\(Pi \\/ {int})")
    public void define_rotation_x_as_Pi_over(String variable, int den) {
        CommonDefinitions.put(
                variable,
                Transformations.rotationX(Math.PI / den)
        );
    }

    @Then("{variable} * {variable} = point\\({double}, Sqrt\\({int})\\/{int}, Sqrt\\({int})\\/{int})")
    public void matrix_multiplied_by_point_equals_point1(String variable1, String variable2, double double1, int num1, int den1, int num2, int den2) {
        Assertions.assertThat(
                CommonDefinitions.getMatrix(variable1).multiply(
                        CommonDefinitions.getTuple(variable2)
                )
        ).isEqualTo(Tuple.point(double1, Math.sqrt(num1) / den1, Math.sqrt(num2) / den2));
    }

    @Then("{variable} * {variable} = point\\(Sqrt\\({int})\\/{int}, {double}, Sqrt\\({int})\\/{int})")
    public void matrix_multiplied_by_point_equals_point2(String variable1, String variable2, int num1, int den1, double double1, int num2, int den2) {
        Assertions.assertThat(
                CommonDefinitions.getMatrix(variable1).multiply(
                        CommonDefinitions.getTuple(variable2)
                )
        ).isEqualTo(Tuple.point(Math.sqrt(num1) / den1, double1, Math.sqrt(num2) / den2));
    }

    @Then("{variable} * {variable} = point\\(-Sqrt\\({int})\\/{int}, Sqrt\\({int})\\/{int}, {double})")
    public void matrix_multiplied_by_point_equals_point3(String variable1, String variable2, int num1, int den1, int num2, int den2, double double1) {
        Assertions.assertThat(
                CommonDefinitions.getMatrix(variable1).multiply(
                        CommonDefinitions.getTuple(variable2)
                )
        ).isEqualTo(Tuple.point(-Math.sqrt(num1) / den1, Math.sqrt(num2) / den2, double1));
    }

    @And("{variable} <- rotation_y\\(Pi \\/ {int})")
    public void define_rotation_y_as_Pi_over(String variable, int den) {
        CommonDefinitions.put(
                variable,
                Transformations.rotationY(Math.PI / den)
        );
    }

    @And("{variable} <- rotation_z\\(Pi \\/ {int})")
    public void define_rotation_z_as_Pi_over(String variable, int den) {
        CommonDefinitions.put(
                variable,
                Transformations.rotationZ(Math.PI / den)
        );
    }


    @Given("{variable} <- shearing\\({int}, {int}, {int}, {int}, {int}, {int})")
    public void define_shearing(String variable, int arg0, int arg1, int arg2, int arg3, int arg4, int arg5) {
        CommonDefinitions.put(
                variable,
                Transformations.shearing(arg0, arg1, arg2, arg3, arg4, arg5)
        );
    }

    @Then("{variable} = point\\({double}, {double}, {double})")
    public void tuple_equals_point(String variable, double double1, double double2, double double3) {
        Assertions.assertThat(
                CommonDefinitions.get(variable)
        ).isEqualTo(
                Tuple.point(double1, double2, double3)
        );
    }

    @When("{variable} <- {variable} * {variable} * {variable}")
    public void define_matrix_as_product_of_three_matrices(String var1, String var2, String var3, String var4) {
        CommonDefinitions.put(var1,
                CommonDefinitions.getMatrix(var2).multiply(
                        CommonDefinitions.getMatrix(var3).multiply(CommonDefinitions.getMatrix(var4))
                )
        );
    }

    @When("{variable} <- view_transform\\({variable}, {variable}, {variable})")
    public void define_transform_as_view_transform(String varTransform, String varFrom, String varTo, String varUp) {
        CommonDefinitions.put(
                varTransform,
                Transformations.viewTransform(
                        CommonDefinitions.getTuple(varFrom),
                        CommonDefinitions.getTuple(varTo),
                        CommonDefinitions.getTuple(varUp)
                )
        );
    }

    @Then("{variable} = scaling\\({int}, {int}, {int})")
    public void assert_transform_equals_scaling(String varTransform, int sx, int sy, int sz) {
        Assertions.assertThat(
                CommonDefinitions.getMatrix(varTransform)
        ).isEqualTo(Transformations.scaling(sx, sy, sz));
    }

    @Then("{variable} = translation\\({int}, {int}, {int})")
    public void assert_transform_equals_translation(String varTransform, int tx, int ty, int tz) {
        Assertions.assertThat(
                CommonDefinitions.getMatrix(varTransform)
        ).isEqualTo(Transformations.translation(tx, ty, tz));
    }
}
