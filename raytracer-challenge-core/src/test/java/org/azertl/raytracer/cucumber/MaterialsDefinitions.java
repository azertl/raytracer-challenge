package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Material;
import org.azertl.raytracer.model.patterns.StripePattern;

public class MaterialsDefinitions {
    @Given("{variable} <- material\\()")
    public void define_material(String varMaterial) {
        CommonDefinitions.put(
                varMaterial,
                new Material()
        );
    }

    @Then("{variable}.color = color\\({int}, {int}, {int})")
    public void assert_material_color_equals_color(String varMaterial, int arg0, int arg1, int arg2) {
        Assertions.assertThat(
                CommonDefinitions.getMaterial(varMaterial).getColor()
        ).isEqualTo(new Color(arg0, arg1, arg2));
    }

    @And("{variable}.ambient = {double}")
    public void assert_material_ambient_equals_value(String varMaterial, double value) {
        Assertions.assertThat(CommonDefinitions.getMaterial(varMaterial).getAmbient()).isEqualTo(value);
    }

    @And("{variable}.diffuse = {double}")
    public void assert_material_diffuse_equals_value(String varMaterial, double value) {
        Assertions.assertThat(CommonDefinitions.getMaterial(varMaterial).getDiffuse()).isEqualTo(value);
    }

    @And("{variable}.specular = {double}")
    public void assert_material_specular_equals_value(String varMaterial, double value) {
        Assertions.assertThat(CommonDefinitions.getMaterial(varMaterial).getSpecular()).isEqualTo(value);
    }

    @And("{variable}.shininess = {double}")
    public void assert_material_shininess_equals_value(String varMaterial, double value) {
        Assertions.assertThat(CommonDefinitions.getMaterial(varMaterial).getShininess()).isEqualTo(value);
    }

    @Then("{variable} = material\\()")
    public void assert_material_equals_default_material(String varMaterial) {
        Assertions.assertThat(
                CommonDefinitions.getMaterial(varMaterial)
        ).isEqualTo(new Material());
    }

    @And("{variable}.ambient <- {double}")
    public void set_material_ambient_as_value(String varMaterial, double value) {
        CommonDefinitions.getMaterial(varMaterial).setAmbient(value);
    }

    @And("{variable} <- true")
    public void define_variable_as_true(String varBoolean) {
        CommonDefinitions.put(varBoolean, true);
    }

    @Given("{variable}.pattern <- stripe_pattern\\(color\\({int}, {int}, {int}), color\\({int}, {int}, {int}))")
    public void define_material_pattern(String varMaterial, int redA, int greenA, int blueA, int redB, int greenB, int blueB) {
        CommonDefinitions.getMaterial(varMaterial).setPattern(
                new StripePattern(new Color(redA, greenA, blueA), new Color(redB, greenB, blueB))
        );
    }

    @And("{variable}.diffuse <- {double}")
    public void set_material_diffuse_as_value(String varMaterial, double value) {
        CommonDefinitions.getMaterial(varMaterial).setDiffuse(value);
    }

    @And("{variable}.specular <- {double}")
    public void set_material_specular_as_value(String varMaterial, double value) {
        CommonDefinitions.getMaterial(varMaterial).setSpecular(value);
    }
}
