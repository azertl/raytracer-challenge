package org.azertl.raytracer.cucumber;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.model.*;
import org.azertl.raytracer.model.patterns.Pattern;
import org.azertl.raytracer.operations.Matrices;

public class RaysDefinitions {

    @When("{variable} <- ray\\({variable}, {variable}\\)")
    public void define_ray_from_variables(String variable1, String variable2, String variable3) {
        CommonDefinitions.put(
                variable1,
                new Ray(CommonDefinitions.getTuple(variable2), CommonDefinitions.getTuple(variable3))
        );
    }

    @Then("{variable}.origin = {variable}")
    public void assert_ray_origin_equals_tuple(String variable1, String variable2) {
        Assertions.assertThat((CommonDefinitions.getRay(variable1)).getOrigin())
                .isEqualTo(CommonDefinitions.getTuple(variable2));
    }

    @Then("{variable}.origin = point\\({double}, {double}, {double})")
    public void assert_ray_origin_equals_point(String variable, double px, double py, double pz) {
        Assertions.assertThat((CommonDefinitions.getRay(variable)).getOrigin())
                .isEqualTo(Tuple.point(px, py, pz));
    }

    @Then("{variable}.direction = vector\\({double}, {double}, {double})")
    public void assert_ray_origin_equals_vector(String variable, double vx, double vy, double vz) {
        Assertions.assertThat((CommonDefinitions.getRay(variable)).getDirection())
                .isEqualTo(Tuple.vector(vx, vy, vz));
    }

    @Then("{variable}.direction = {variable}")
    public void assert_ray_direction_equals_tuple(String variable1, String variable2) {
        Assertions.assertThat((CommonDefinitions.getRay(variable1)).getDirection())
                .isEqualTo(CommonDefinitions.getTuple(variable2));
    }

    @Given("{variable} <- ray\\(point\\({double}, {double}, {double}), vector\\({double}, {double}, {double}))")
    public void define_ray_from_point_and_vector(String variable, double px, double py, double pz, double vx, double vy, double vz) {
        CommonDefinitions.put(
                variable,
                new Ray(
                        Tuple.point(px, py, pz),
                        Tuple.vector(vx, vy, vz)
                )
        );
    }

    @Then("position\\({variable}, {double}) = point\\({double}, {double}, {double})")
    public void assert_position_of_ray_and_t_equals_point(String rayVar, double t, double px, double py, double pz) {
        Assertions.assertThat(
                CommonDefinitions.getRay(rayVar).getPosition(t)
        ).isEqualTo(
                Tuple.point(px, py, pz)
        );
    }

    @And("{variable} <- sphere\\()")
    public void define_sphere(String varSphere) {
        CommonDefinitions.put(
                varSphere,
                new Sphere()
        );
    }

    @Then("{variable}.count = {int}")
    public void assert_intersections_count_equals(String varIntersections, int count) {
        Assertions.assertThat(CommonDefinitions.getIntersections(varIntersections).count()).isEqualTo(count);
    }

    @And("{variable}[{int}] = {double}")
    public void assert_intersection_equals(String varIntersections, int idx, double expectedVal) {
        Assertions.assertThat(CommonDefinitions.getIntersections(varIntersections).get(idx).getTime()).isEqualTo(expectedVal);
    }

    @When("{variable} <- intersection\\({double}, {variable})")
    public void define_intersection(String varIntersection, double t, String varSphere) {
        CommonDefinitions.put(
                varIntersection,
                new Intersection(t, CommonDefinitions.getSphere(varSphere))
        );
    }

    @Then("{variable}.t = {double}")
    public void assert_intersection_time_equals_value(String varIntersection, double expectedValue) {
        Assertions.assertThat(CommonDefinitions.getIntersection(varIntersection).getTime()).isEqualTo(expectedValue);
    }

    @And("{variable}.object = {variable}")
    public void assert_intersection_object_equals_object(String varIntersection, String varObject) {
        Assertions.assertThat(CommonDefinitions.getIntersection(varIntersection).getObject()).isEqualTo(CommonDefinitions.getSphere(varObject));
    }

    @When("{variable} <- intersections\\({variable}, {variable})")
    public void define_intersections_of_two(String varIntersections, String varInt1, String varInt2) {
        CommonDefinitions.put(
                varIntersections,
                new Intersections(
                        CommonDefinitions.getIntersection(varInt1),
                        CommonDefinitions.getIntersection(varInt2)
                )
        );
    }


    @When("{variable} <- intersections\\({variable}, {variable}, {variable}, {variable})")
    public void define_intersections_of_four(String varIntersections, String varInt1, String varInt2, String varInt3, String varInt4) {
        CommonDefinitions.put(
                varIntersections,
                new Intersections(
                        CommonDefinitions.getIntersection(varInt1),
                        CommonDefinitions.getIntersection(varInt2),
                        CommonDefinitions.getIntersection(varInt3),
                        CommonDefinitions.getIntersection(varInt4)
                )
        );
    }

    @And("{variable}[{int}].t = {double}")
    public void assert_intersection_t_equals(String varIntersections, int idx, double expectedValue) {
        Assertions.assertThat(
                CommonDefinitions.getIntersections(varIntersections).get(idx).getTime()
        ).isEqualTo(expectedValue);
    }

    @And("{variable}[{int}].object = {variable}")
    public void assert_intersection_object_equals(String varIntersections, int idx, String varShape) {
        Assertions.assertThat(
                CommonDefinitions.getIntersections(varIntersections).get(idx).getObject()
        ).isEqualTo(
                CommonDefinitions.getShape(varShape)
        );
    }

    @When("{variable} <- hit\\({variable})")
    public void define_hit_of_intersections(String varIntersection, String varIntersections) {
        CommonDefinitions.put(
                varIntersection,
                CommonDefinitions.getIntersections(varIntersections).hit()
        );
    }

    @Then("{variable} is nothing")
    public void assert_object_is_null(String varIntersection) {
        Assertions.assertThat(
                CommonDefinitions.getIntersections(varIntersection)
        ).isNull();
    }

    @When("{variable} <- transform\\({variable}, {variable})")
    public void define_ray_transformed(String varRayTransformed, String varRayOriginal, String varMatrix) {
        CommonDefinitions.put(
                varRayTransformed,
                CommonDefinitions.getRay(varRayOriginal).transform(CommonDefinitions.getMatrix(varMatrix))
        );
    }

    @Then("{variable}.transform = identity_matrix")
    public void assert_object_default_transformation(String varObject) {
        Object obj = CommonDefinitions.get(varObject);

        if (obj instanceof Shape shape) {
            Assertions.assertThat(shape.getTransform()).isEqualTo(Matrices.identityMatrix());
        } else if (obj instanceof Camera cam) {
            Assertions.assertThat(cam.getTransform()).isEqualTo(Matrices.identityMatrix());
        } else if (obj instanceof Pattern pattern) {
            Assertions.assertThat(pattern.getTransform()).isEqualTo(Matrices.identityMatrix());
        } else {
            Assertions.fail("Unknown type " + obj.getClass().getName());
        }
    }

    @Then("{variable}.transform = {variable}")
    public void assert_sphere_transform_equals_matrix(String varSphere, String varMatrix) {
        Assertions.assertThat(CommonDefinitions.getSphere(varSphere).getTransform())
                .isEqualTo(CommonDefinitions.getMatrix(varMatrix));
    }

    @And("{variable}.direction = vector\\(Sqrt\\({int})\\/{int}, {int}, -Sqrt\\({int})\\/{int})")
    public void assert_ray_direction_equals_vector(String varRay, int nx, int dx, int y, int nz, int dz) {
        Assertions.assertThat(CommonDefinitions.getRay(varRay).getDirection())
                .isEqualTo(Tuple.vector(Math.sqrt(nx) / dx, y, -Math.sqrt(nz) / dz));
    }
}
