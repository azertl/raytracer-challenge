package org.azertl.raytracer.cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.azertl.raytracer.mock.TestShape;
import org.azertl.raytracer.model.Intersections;
import org.azertl.raytracer.model.patterns.Pattern;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;
import org.azertl.raytracer.operations.Transformations;

public class ShapesDefinitions {
    @Given("{variable} <- test_shape\\()")
    public void define_test_shape(String varShape) {
        CommonDefinitions.put(
                varShape,
                new TestShape()
        );
    }

    @When("set_transform\\({variable}, translation\\({double}, {double}, {double}))")
    public void set_shape_transform_to_translation(String varShape, double trX, double trY, double trZ) {
        CommonDefinitions.getShape(varShape)
                .setTransform(Transformations.translation(trX, trY, trZ));
    }

    @Then("{variable}.transform = translation\\({int}, {int}, {int})")
    public void assert_object_transformation_equals_translation(String varShape, int tX, int tY, int tZ) {
        Object obj = CommonDefinitions.get(varShape);

        if (obj instanceof Shape shape) {
            Assertions.assertThat(shape.getTransform()).isEqualTo(Transformations.translation(tX, tY, tZ));
        } else if (obj instanceof Pattern pattern) {
            Assertions.assertThat(pattern.getTransform()).isEqualTo(Transformations.translation(tX, tY, tZ));
        }  else {
            Assertions.fail("Unknown type " + obj.getClass().getName());
        }
    }

    @When("{variable} <- {variable}.material")
    public void define_variable_as_shape_material(String varMaterial, String varShape) {
        CommonDefinitions.put(
                varMaterial,
                CommonDefinitions.getShape(varShape).getMaterial()
        );
    }

    @When("{variable}.material <- {variable}")
    public void set_shape_material(String varShape, String varMaterial) {
        CommonDefinitions.getShape(varShape)
                .setMaterial(CommonDefinitions.getMaterial(varMaterial));
    }

    @Then("{variable}.material = {variable}")
    public void assert_shape_material_equals_variable(String varShape, String varMaterial) {
        Assertions.assertThat(
                CommonDefinitions.getShape(varShape).getMaterial()
        ).isEqualTo(CommonDefinitions.getMaterial(varMaterial));
    }

    @When("set_transform\\({variable}, scaling\\({double}, {double}, {double}))")
    public void set_shape_transform_to_scaling(String varShape, double scX, double scY, double scZ) {
        CommonDefinitions.getShape(varShape)
                .setTransform(Transformations.scaling(scX, scY, scZ));
    }


    @When("{variable} <- intersect\\({variable}, {variable})")
    public void define_intersections_of_shape_and_ray(String varIntersections, String varShape, String varRay) {
        Intersections intersections = CommonDefinitions.getShape(varShape).intersect(CommonDefinitions.getRay(varRay));

        CommonDefinitions.put(
                varIntersections,
                intersections
        );
    }

    @Then("{variable}.saved_ray.origin = point\\({int}, {int}, {double})")
    public void assert_shape_saved_ray_origin_equals_point(String varShape, int x, int y, double z) {
        TestShape testShape = (TestShape) CommonDefinitions.getShape(varShape);
        Assertions.assertThat(testShape.getSavedRay().getOrigin())
                .isEqualTo(Tuple.point(x, y, z));
    }

    @Then("{variable}.saved_ray.direction = vector\\({int}, {int}, {double})")
    public void assert_shape_saved_ray_direction_equals_vector(String varShape, int x, int y, double z) {
        TestShape testShape = (TestShape) CommonDefinitions.getShape(varShape);
        Assertions.assertThat(testShape.getSavedRay().getDirection())
                .isEqualTo(Tuple.vector(x, y, z));
    }

    @When("{variable} <- normal_at\\({variable}, point\\({double}, {double}, {double}))")
    public void define_normal_at_point_on_shape(String varVector, String varShape, double px, double py, double pz) {
        CommonDefinitions.put(
                varVector,
                CommonDefinitions.getShape(varShape).getNormalAt(Tuple.point(px, py, pz))
        );
    }

    @When("set_transform\\({variable}, {variable})")
    public void set_shape_transform_to_matrix(String varShape, String varMatrix) {
        CommonDefinitions.getShape(varShape).setTransform(CommonDefinitions.getMatrix(varMatrix));
    }

    @When("{variable} <- normal_at\\({variable}, point\\({int}, Sqrt\\({int})\\/{int}, -Sqrt\\({int})\\/{int}))")
    public void define_normal_at_point_on_shape(String varVector, String varShape, int arg0, int arg1, int arg2, int arg3, int arg4) {
        CommonDefinitions.put(
                varVector,
                CommonDefinitions.getShape(varShape).getNormalAt(Tuple.point(arg0, Math.sqrt(arg1) / arg2, -Math.sqrt(arg3) / arg4))
        );
    }

    @Then("{variable} is a shape")
    public void assert_var_is_a_shape(String varShape) {
        Assertions.assertThat(
                CommonDefinitions.getShape(varShape)
        ).isInstanceOf(Shape.class);
    }

    @When("{variable} <- local_normal_at\\({variable}, point\\({int}, {int}, {int})")
    public void define_local_normal_at_point(String varVector, String varShape, int x, int y, int z) {
        CommonDefinitions.put(
                varVector,
                CommonDefinitions.getShape(varShape).getLocalNormalAt(Tuple.point(x, y, z))
        );
    }
}
