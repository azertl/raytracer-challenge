package org.azertl.raytracer.mock;

import org.azertl.raytracer.model.Intersections;
import org.azertl.raytracer.model.Ray;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

public class TestShape extends Shape {

    private Ray savedRay;

    public Intersections intersect(Ray ray) {
        // First transform the ray to adapt to non-unit sphere
        savedRay = ray.transform(getTransform().inverse());

        return getLocalIntersections(savedRay);
    }

    @Override
    public Intersections getLocalIntersections(Ray transformedRay) {
        return null;
    }

    @Override
    public Tuple getLocalNormalAt(Tuple localPoint) {
        return Tuple.vector(localPoint.x, localPoint.y, localPoint.z);
    }

    public Ray getSavedRay() {
        return savedRay;
    }
}
