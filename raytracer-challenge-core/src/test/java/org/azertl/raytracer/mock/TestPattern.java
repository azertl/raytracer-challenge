package org.azertl.raytracer.mock;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.patterns.Pattern;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

public class TestPattern extends Pattern {
    @Override
    public Color getPatternAtShape(Shape shape, Tuple point) {
        Tuple objectSpacePoint = shape.getTransform().inverse().multiply(point);
        Tuple patternSpacePoint = getTransform().inverse().multiply(objectSpacePoint);
        return new Color(patternSpacePoint.x, patternSpacePoint.y, patternSpacePoint.z);
    }
}
