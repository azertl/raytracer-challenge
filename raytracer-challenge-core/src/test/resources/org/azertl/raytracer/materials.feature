Feature: Materials features

  Background:
    Given m <- material()
    And position <- point(0, 0, 0)

  Scenario: The default material
    Then m.color = color(1, 1, 1)
    And m.ambient = 0.1
    And m.diffuse = 0.9
    And m.specular = 0.9
    And m.shininess = 200.0

  Scenario: Lighting with the eye between the light and the surface
    Given eyev <- vector(0, 0, -1)
    And normalv <- vector(0, 0, -1)
    And light <- point_light(point(0, 0, -10), color(1, 1, 1))
    When result <- lighting(m, light, position, eyev, normalv)
    Then result = color(1.9, 1.9, 1.9)

  Scenario: Lighting with the eye between light and surface, eye offset 45°
    Given eyev <- vector(0, Sqrt(2)/2, -Sqrt(2)/2)
    And normalv <- vector(0, 0, -1)
    And light <- point_light(point(0, 0, -10), color(1, 1, 1))
    When result <- lighting(m, light, position, eyev, normalv)
    Then result = color(1.0, 1.0, 1.0)

  Scenario: Lighting with eye opposite surface, light offset 45°
    Given eyev <- vector(0, 0, -1)
    And normalv <- vector(0, 0, -1)
    And light <- point_light(point(0, 10, -10), color(1, 1, 1))
    When result <- lighting(m, light, position, eyev, normalv)
    Then result = color(0.7364, 0.7364, 0.7364)

  Scenario: Lighting with eye in the path of the reflection vector
    Given eyev <- vector(0, -Sqrt(2)/2, -Sqrt(2)/2)
    And normalv <- vector(0, 0, -1)
    And light <- point_light(point(0, 10, -10), color(1, 1, 1))
    When result <- lighting(m, light, position, eyev, normalv)
    Then result = color(1.6364, 1.6364, 1.6364)

  Scenario: Lighting with the light behind the surface
    Given eyev <- vector(0, 0, -1)
    And normalv <- vector(0, 0, -1)
    And light <- point_light(point(0, 0, 10), color(1, 1, 1))
    When result <- lighting(m, light, position, eyev, normalv)
    Then result = color(0.1, 0.1, 0.1)

  Scenario: Lighting with the surface in shadow
    Given eyev <- vector(0, 0, -1)
    And normalv <- vector(0, 0, -1)
    And light <- point_light(point(0, 0, -10), color(1, 1, 1))
    And inShadow <- true
    When result <- lighting(m, light, position, eyev, normalv, inShadow)
    Then result = color(0.1, 0.1, 0.1)

  Scenario: Lighting with a pattern applied
    Given m.pattern <- stripe_pattern(color(1, 1, 1), color(0, 0, 0))
    And m.ambient <- 1
    And m.diffuse <- 0
    And m.specular <- 0
    And eyev <- vector(0, 0, -1)
    And normalv <- vector(0, 0, -1)
    And light <- point_light(point(0, 0, -10), color(1, 1, 1))
    When c1 <- lighting(m, light, point(0.9, 0, 0), eyev, normalv, false)
    And c2 <- lighting(m, light, point(1.1, 0, 0), eyev, normalv, false)
    Then c1 = color(1, 1, 1)
    And c2 = color(0, 0, 0)