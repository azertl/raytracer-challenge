package org.azertl.raytracer.model.patterns;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

public class RadialGradientPattern extends Pattern {
    private Color colorA;
    private Color colorB;

    public RadialGradientPattern(Color colorA, Color colorB) {
        super();
        this.colorA = colorA;
        this.colorB = colorB;
    }

    @Override
    public Color getPatternAtShape(Shape shape, Tuple point) {
        Tuple colorDist = colorB.subtract(colorA);

        double distance = Math.sqrt(point.x * point.x + point.z * point.z);

        double floorDist = Math.floor(distance);

        double distInGrad = distance - floorDist;

        return floorDist % 2 == 0 ?
                new Color(colorA.add(colorDist.multiply(distInGrad))) :
                new Color(colorB.subtract(colorDist.multiply(distInGrad)));
    }
}
