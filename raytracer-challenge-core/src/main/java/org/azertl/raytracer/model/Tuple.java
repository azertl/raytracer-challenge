package org.azertl.raytracer.model;

import org.azertl.raytracer.utils.NumberUtils;

public class Tuple {
    public static final double W_POINT = 1.0;
    public static final double W_VECTOR = 0.0;
    public static final double W_COLOR = -1.0;

    public final double x, y, z, w;

    public Tuple(double x, double y, double z, double w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public double magnitude() {
        return Math.sqrt(x * x + y * y + z * z + w * w);
    }

    public Tuple negate() {
        return new Tuple(-x, -y, -z, -w);
    }

    public double dot(Tuple other) {
        return x * other.x + y * other.y + z * other.z + w * other.w;
    }

    public Tuple multiply(double scalar) {
        return new Tuple(x * scalar, y * scalar, z * scalar, w * scalar);
    }

    public Tuple add(Tuple other) {
        return new Tuple(x + other.x, y + other.y, z + other.z, w + other.w);
    }

    public Tuple subtract(Tuple other) {
        return new Tuple(x - other.x, y - other.y, z - other.z, w - other.w);
    }

    public boolean isPoint() {
        return w == 1.0;
    }

    public boolean isVector() {
        return w == 0.0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tuple)) return false;

        return approxEquals((Tuple) o);
    }

    public boolean approxEquals(Tuple tuple) {
        if (!NumberUtils.equalsDouble(x, tuple.x)) return false;
        if (!NumberUtils.equalsDouble(y, tuple.y)) return false;
        return (NumberUtils.equalsDouble(z, tuple.z));
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(w);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        if (isPoint()) {
            return String.format("Point(%.2f, %.2f, %.2f)", x, y, z);
        }
        if (isVector()) {
            return String.format("Vector[%.2f, %.2f, %.2f]", x, y, z);
        }
        return String.format("Tuple{%.2f, %.2f, %.2f, %.2f}", x, y, z, w);
    }

    public static Tuple vector(double x, double y, double z) {
        return new Tuple(x, y, z, W_VECTOR);
    }

    public static Tuple vector(Tuple tuple) {
        return new Tuple(tuple.x, tuple.y, tuple.z, W_VECTOR);
    }

    public static Tuple point(double x, double y, double z) {
        return new Tuple(x, y, z, W_POINT);
    }

    public Tuple normalize() {
        double magnitude = this.magnitude();
        return new Tuple(x / magnitude, y / magnitude, z / magnitude, w / magnitude);
    }

    /**
     * Reflects in vector (this) into the normal vector.
     */
    public Tuple reflect(Tuple normal) {
        return subtract(
                normal
                        .multiply(2)
                        .multiply(dot(normal)));
    }
}
