package org.azertl.raytracer.model;

import org.azertl.raytracer.utils.NumberUtils;

public class Plane extends Shape {
    @Override
    public Intersections getLocalIntersections(Ray transformedRay) {
        if (Math.abs(transformedRay.getDirection().y) < NumberUtils.EPSILON) {
            return new Intersections(); // No intersection
        }

        // There is an intersection !
        double time = -transformedRay.getOrigin().y / transformedRay.getDirection().y;

        return new Intersections(new Intersection(time, this));
    }

    @Override
    public Tuple getLocalNormalAt(Tuple localPoint) {
        return Tuple.vector(0, 1, 0);
    }
}
