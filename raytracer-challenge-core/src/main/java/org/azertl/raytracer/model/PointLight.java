package org.azertl.raytracer.model;

import java.util.Objects;

public class PointLight {
    private final Tuple position;
    private final Color intensity;

    public PointLight(Tuple position, Color intensity) {
        this.position = position;
        this.intensity = intensity;
    }

    public Tuple getPosition() {
        return position;
    }

    public Color getIntensity() {
        return intensity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PointLight that = (PointLight) o;

        if (!Objects.equals(position, that.position)) return false;
        return Objects.equals(intensity, that.intensity);
    }

    @Override
    public int hashCode() {
        int result = position != null ? position.hashCode() : 0;
        result = 31 * result + (intensity != null ? intensity.hashCode() : 0);
        return result;
    }
}
