package org.azertl.raytracer.model;

import org.azertl.raytracer.operations.Matrices;
import org.azertl.raytracer.operations.Transformations;

public class Camera {
    private final int hsize;

    private final int vsize;

    private final double fieldOfView;
    private Matrix transformation;
    private Double halfWidth = null;
    private Double halfHeight = null;

    public Camera(int hsize, int vsize, double fieldOfView) {
        this.hsize = hsize;
        this.vsize = vsize;
        this.fieldOfView = fieldOfView;
        this.transformation = Matrices.identityMatrix();
    }

    public double getHsize() {
        return hsize;
    }

    public double getVsize() {
        return vsize;
    }

    public double getFieldOfView() {
        return fieldOfView;
    }

    public Matrix getTransform() {
        return transformation;
    }

    public double getPixelSize() {
        // TODO Why ? cf p102
        computesHalvesIfNecessary();
        return (halfWidth * 2) / hsize;
    }

    private void computesHalvesIfNecessary() {
        if (halfHeight == null || halfWidth == null) {
            double halfView = Math.tan(fieldOfView / 2);
            double aspect = ((double) hsize) / vsize;
            if (aspect >= 1) {
                halfWidth = halfView;
                halfHeight = halfView / aspect;
            } else {
                halfWidth = halfView * aspect;
                halfHeight = halfView;
            }
        }
    }

    public Ray getRayForPixel(int px, int py) {
        // the offset from the edge of the camera to the pixel's center
        double xOffset = (px + 0.5) * getPixelSize();
        double yOffset = (py + 0.5) * getPixelSize();

        // the untransformed coordinates of the pixel in world space.
        // remember that the camera looks toward -z so +x is to the *left*.
        double worldX = halfWidth - xOffset;
        double worldY = halfHeight - yOffset;

        // using the camera matrix, transform the canvas point and the origin
        // and then compute the ray's direction vector.
        // remember that the camera is at z=-1
        Tuple pixel = transformation.inverse().multiply(Tuple.point(worldX, worldY, -1));
        Tuple origin = transformation.inverse().multiply(Tuple.point(0, 0, 0));
        Tuple direction = pixel.subtract(origin).normalize();

        return new Ray(origin, direction);
    }

    public void setTransform(Matrix transformation) {
        this.transformation = transformation;
    }

    public void setViewTransform(Tuple from, Tuple to, Tuple up) {
        setTransform(Transformations.viewTransform(from, to, up));
    }

    public Canvas render(World world) {
        return render(world, false);
    }

    public Canvas render(World world, boolean showProgress) {
        Canvas image = new Canvas(hsize, vsize);

        for (int y = 0; y < vsize - 1; y++) {
            for (int x = 0; x < hsize - 1; x++) {
                if (showProgress) {
                    int total = vsize * hsize;
                    int current = hsize * y + x;
                    int progress = (100 * current) / total;

                    System.out.print( progress + " %\r");
                }
                Ray ray = getRayForPixel(x, y);
                Color color = world.colorAt(ray);
                image.setPixel(x, y, color);
            }
        }
        return image;
    }
}
