package org.azertl.raytracer.model;

import org.azertl.raytracer.model.patterns.Pattern;

public class Material {
    private Color color;

    private double ambient;
    private double diffuse;
    private double specular;
    private double shininess;

    private Pattern pattern;

    public Material() {
        this.color = new Color(1, 1, 1);
        this.ambient = 0.1;
        this.diffuse = 0.9;
        this.specular = 0.9;
        this.shininess = 200.0;
    }

    public double getAmbient() {
        return ambient;
    }

    public void setAmbient(double value) {
        this.ambient = value;
    }

    public Color getColor() {
        return color;
    }

    public double getDiffuse() {
        return diffuse;
    }

    public double getSpecular() {
        return specular;
    }

    public double getShininess() {
        return shininess;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setColor(double red, double green, double blue) {
        this.color = new Color(red, green, blue);
    }

    public void setDiffuse(double diffuse) {
        this.diffuse = diffuse;
    }

    public void setSpecular(double specular) {
        this.specular = specular;
    }

    public void setShininess(double shininess) {
        this.shininess = shininess;
    }

    public Color lighting(PointLight pointLight, Shape object, Tuple position, Tuple eyeVector, Tuple normalVector, boolean inShadow) {
        Color color = this.color;
        if (pattern != null) {
            color = pattern.getPatternAtShape(object, position);
        }

        // Combine the surface color with the light's color/intensity
        Tuple effectiveColor = color.hadamardProduct(pointLight.getIntensity());

        // Find the direction to the light source
        Tuple lightVector = pointLight.getPosition().subtract(position).normalize();

        // Compute the ambient contribution
        Tuple ambientContribution = effectiveColor.multiply(ambient);

        // If in shadow, then only ambient contribution is useful
        if (inShadow) {
            return new Color(ambientContribution);
        }

        // light_dot_normal represents the cosine of the angle between
        // the light vector and the normal vector. A negative number means
        // the light is on the other side of the surface
        double lightDotNormal = lightVector.dot(normalVector);

        // Default values (if lightDotNormal < 0)
        Tuple diffuseContribution = new Color(0, 0, 0);
        Tuple specularContribution = new Color(0, 0, 0);

        if (lightDotNormal >= 0) {
            // Compute the diffuse contribution
            diffuseContribution = effectiveColor.multiply(diffuse).multiply(lightDotNormal);

            // reflectDotEye represents the cosine of the angle between the reflection
            // vector and the eye vector. A negative number means the light reflects
            // away from the eye
            Tuple reflectVector = lightVector.negate().reflect(normalVector);
            double reflectDotEye = reflectVector.dot(eyeVector);

            if (reflectDotEye > 0) {
                // Compute the specular contribution
                double factor = Math.pow(reflectDotEye, shininess);
                specularContribution = pointLight.getIntensity().multiply(specular).multiply(factor);
            } // else specularContribution = black
        }

        return new Color(ambientContribution.add(diffuseContribution).add(specularContribution));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Material material = (Material) o;

        if (Double.compare(ambient, material.ambient) != 0) return false;
        if (Double.compare(diffuse, material.diffuse) != 0) return false;
        if (Double.compare(specular, material.specular) != 0) return false;
        if (Double.compare(shininess, material.shininess) != 0) return false;
        return color.equals(material.color);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = color.hashCode();
        temp = Double.doubleToLongBits(ambient);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(diffuse);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(specular);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(shininess);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public Pattern getPattern() {
        return pattern;
    }
}
