package org.azertl.raytracer.model;

import java.util.Objects;

public final class Intersection {
    private final double time;
    private final Shape object;

    public Intersection(double time, Shape object) {
        this.time = time;
        this.object = object;
    }

    public double getTime() {
        return time;
    }

    public Shape getObject() {
        return object;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Intersection) obj;
        return Double.doubleToLongBits(this.time) == Double.doubleToLongBits(that.time) &&
                Objects.equals(this.object, that.object);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, object);
    }

    @Override
    public String toString() {
        return "Intersection[" +
                "time=" + time + ", " +
                "object=" + object + ']';
    }

}
