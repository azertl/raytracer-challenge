package org.azertl.raytracer.model.patterns;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

public class RingPattern extends Pattern {
    private Color colorA;
    private Color colorB;

    public RingPattern(Color colorA, Color colorB) {
        super();
        this.colorA = colorA;
        this.colorB = colorB;
    }

    @Override
    public Color getPatternAtShape(Shape shape, Tuple point) {
        return Math.floor(Math.sqrt(point.x * point.x + point.z * point.z)) % 2 == 0 ?
                colorA :
                colorB;
    }
}
