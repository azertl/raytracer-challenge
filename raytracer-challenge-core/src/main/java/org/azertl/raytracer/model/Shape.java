package org.azertl.raytracer.model;

import org.azertl.raytracer.model.patterns.Pattern;
import org.azertl.raytracer.operations.Matrices;

import java.util.Objects;

public abstract class Shape {
    private Matrix transformation;

    private Material material;

    public Shape() {
        this.transformation = Matrices.identityMatrix();
        this.material = new Material();
    }

    public Matrix getTransform() {
        return transformation;
    }

    public Shape setTransform(Matrix matrix) {
        transformation = matrix;
        return this;
    }

    public Material getMaterial() {
        return material;
    }

    public Shape setColor(Color color) {
        getMaterial().setColor(color);
        return this;
    }

    public Shape setPattern(Pattern pattern) {
        getMaterial().setPattern(pattern);
        return this;
    }

    public Shape setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public Intersections intersect(Ray ray) {
        // First transform the ray to adapt to non-unit sphere
        Ray localRay = ray.transform(getTransform().inverse());

        return getLocalIntersections(localRay);
    }

    public abstract Intersections getLocalIntersections(Ray transformedRay);

    public Tuple getNormalAt(Tuple point) {
        // Convert world point into object point
        Tuple localPoint = getTransform()
                .inverse()
                .multiply(point);

        Tuple localNormal = getLocalNormalAt(localPoint);

        Tuple worldNormal = Tuple.vector(
                getTransform()
                        .inverse()
                        .transpose()
                        .multiply(localNormal)
        );

        return worldNormal.normalize();
    }

    public abstract Tuple getLocalNormalAt(Tuple localPoint);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shape shape = (Shape) o;

        if (!Objects.equals(transformation, shape.transformation))
            return false;
        return Objects.equals(material, shape.material);
    }

    @Override
    public int hashCode() {
        int result = transformation != null ? transformation.hashCode() : 0;
        result = 31 * result + (material != null ? material.hashCode() : 0);
        return result;
    }
}
