package org.azertl.raytracer.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class World {
    private final List<Shape> objects = new ArrayList<>();
    // TODO Allow having multiple light sources, see p96
    private PointLight lightSource;
    private boolean allowShadows = true;

    public static World createDefaultWorld() {
        World world = new World();
        world.lightSource = new PointLight(Tuple.point(-10, 10, -10), new Color(1, 1, 1));

        // Sphere 1
        Material material = new Material();
        material.setColor(0.8, 1.0, 0.6);
        material.setDiffuse(0.7);
        material.setSpecular(0.2);
        Sphere sphere1 = new Sphere();
        sphere1.setMaterial(material);
        world.objects.add(sphere1);

        // Sphere2
        Sphere sphere2 = new Sphere();
        sphere2.setTransform(
                new Transformation().scaling(0.5, 0.5, 0.5)
        );
        world.objects.add(sphere2);

        return world;
    }

    public Collection<Shape> getObjects() {
        return objects;
    }

    public PointLight getLightSource() {
        return lightSource;
    }

    public Shape getObject(int i) {
        return objects.get(i);
    }

    public void setLightSource(PointLight pointLight) {
        this.lightSource = pointLight;
    }

    public Color shadeHit(PreparedComputations comps) {
        boolean shadowed = allowShadows && isShadowed(comps.getOverPoint());
        return comps.getObject().getMaterial()
                .lighting(getLightSource(), comps.getObject(), comps.getPoint(), comps.getEyev(), comps.getNormalv(), shadowed);
    }

    public Color colorAt(Ray ray) {
        Intersections intersections = ray.intersect(this);
        Intersection hit = intersections.hit();
        if (hit == null) {
            return new Color(0, 0, 0);
        }

        return shadeHit(PreparedComputations.build(hit, ray));
    }

    public void addObject(Shape object) {
        this.objects.add(object);
    }

    public Sphere createSphere() {
        Sphere sphere = new Sphere();
        addObject(sphere);
        return sphere;
    }

    public Plane createPlane() {
        Plane plane = new Plane();
        addObject(plane);
        return plane;
    }

    public boolean isShadowed(Tuple point) {
        Tuple v = lightSource.getPosition().subtract(point);
        double distance = v.magnitude();
        Tuple direction = v.normalize();

        Ray r = new Ray(point, direction);
        Intersections intersections = r.intersect(this);

        Intersection h = intersections.hit();
        return h != null && h.getTime() < distance;
    }

    public void setAllowShadows(boolean allowShadows) {
        this.allowShadows = allowShadows;
    }
}
