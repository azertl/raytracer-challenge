package org.azertl.raytracer.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Intersections {
    private final List<Intersection> values;

    public Intersections(Intersection... values) {
        this.values = new ArrayList<>(List.of(values));
        sortValues();
    }

    private void sortValues() {
        this.values.sort(getComparator());
    }

    public Intersection get(int idx) {
        return values.get(idx);
    }

    public int count() {
        return values.size();
    }

    public boolean isEmpty() {
        return values.isEmpty();
    }

    public Intersection hit() {
        return values.stream()
                .filter(intersection -> intersection.getTime() >= 0)
                .min(getComparator())
                .orElse(null);
    }

    private static Comparator<Intersection> getComparator() {
        return Comparator.comparing(Intersection::getTime);
    }

    public void addAll(Intersections intersect) {
        values.addAll(intersect.values);
        sortValues();
    }
}
