package org.azertl.raytracer.model;

public class Color extends Tuple {

    public Color(Tuple tuple) {
        this(tuple.x, tuple.y, tuple.z);
    }

    public Color(double red, double green, double blue) {
        super(red, green, blue, W_COLOR);
    }

    public double red() {
        return x;
    }

    public double green() {
        return y;
    }

    public double blue() {
        return z;
    }

    public Color hadamardProduct(Tuple other) {
        return new Color(x * other.x, y * other.y, z * other.z);
    }
}
