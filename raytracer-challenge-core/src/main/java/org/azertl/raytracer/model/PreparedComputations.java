package org.azertl.raytracer.model;

import org.azertl.raytracer.utils.NumberUtils;

public class PreparedComputations {
    private final double time;

    private final Shape object;

    /**
     * Point of intersection ray-object
     */
    private final Tuple point;
    private final Tuple eyev;
    private final Tuple normalv;
    private final boolean inside;
    private Tuple overPoint;

    public static PreparedComputations build(Intersection intersection, Ray ray) {
        double time = intersection.getTime();
        Shape object = intersection.getObject();
        Tuple point = ray.getPosition(time);
        Tuple normalv = object.getNormalAt(point);
        Tuple eyev = ray.getDirection().negate();
        boolean inside = false;

        if (normalv.dot(eyev) < 0) {
            inside = true;
            normalv = normalv.negate();
        }

        // After computing and (if appropriate) negating the normal vector
        Tuple overPoint = point.add(normalv.multiply(NumberUtils.EPSILON));

        return new PreparedComputations(time, object, point, eyev, normalv, inside, overPoint);
    }

    private PreparedComputations(double time, Shape object, Tuple point, Tuple eyev, Tuple normalv, boolean inside, Tuple overPoint) {
        this.time = time;
        this.object = object;
        this.point = point;
        this.eyev = eyev;
        this.normalv = normalv;
        this.inside = inside;
        this.overPoint = overPoint;
    }

    public double getTime() {
        return time;
    }

    public Shape getObject() {
        return object;
    }

    public Tuple getPoint() {
        return point;
    }

    public Tuple getEyev() {
        return eyev;
    }

    public Tuple getNormalv() {
        return normalv;
    }

    public boolean isInside() {
        return inside;
    }

    public Tuple getOverPoint() {
        return overPoint;
    }
}
