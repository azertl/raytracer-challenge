package org.azertl.raytracer.model.patterns;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

public class GradientPattern extends Pattern {
    private Color colorA;
    private Color colorB;

    public GradientPattern(Color colorA, Color colorB) {
        super();
        this.colorA = colorA;
        this.colorB = colorB;
    }

    @Override
    public Color getPatternAtShape(Shape shape, Tuple point) {
        Tuple distance = colorB.subtract(colorA);
        double fraction = point.x - Math.floor(point.x);
        return new Color(colorA.add(distance.multiply(fraction)));
    }
}
