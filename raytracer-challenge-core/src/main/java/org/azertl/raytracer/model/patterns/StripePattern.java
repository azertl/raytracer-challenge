package org.azertl.raytracer.model.patterns;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

public class StripePattern extends Pattern {
    private final Color colorA;
    private final Color colorB;

    public StripePattern(Color colorA, Color colorB) {
        this.colorA = colorA;
        this.colorB = colorB;
    }

    public Color getColorA() {
        return colorA;
    }

    public Color getColorB() {
        return colorB;
    }

    public Color getStripeAt(Tuple point) {
        double x = point.x;
        double floor = Math.floor(x);
        return ((int) floor) % 2 == 0 ? colorA : colorB;
    }

    @Override
    public Color getPatternAtShape(Shape shape, Tuple point) {
        Tuple objectSpacePoint = shape.getTransform().inverse().multiply(point);
        Tuple patternSpacePoint = getTransform().inverse().multiply(objectSpacePoint);
        return getStripeAt(patternSpacePoint);
    }
}
