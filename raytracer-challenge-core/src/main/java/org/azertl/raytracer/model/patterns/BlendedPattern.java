package org.azertl.raytracer.model.patterns;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

public class BlendedPattern extends Pattern{
    private Pattern pattern1, pattern2;

    public BlendedPattern(Pattern pattern1, Pattern pattern2) {
        super();
        this.pattern1 = pattern1;
        this.pattern2 = pattern2;
    }

    @Override
    public Color getPatternAtShape(Shape shape, Tuple point) {
        return new Color(
                pattern1.getPatternAtShape(shape, point)
                        .add(pattern2.getPatternAtShape(shape, point))
                        .multiply(0.5)
        );
    }
}
