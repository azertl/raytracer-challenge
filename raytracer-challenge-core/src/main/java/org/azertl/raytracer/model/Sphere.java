package org.azertl.raytracer.model;

public class Sphere extends Shape {

    public Sphere() {
        super();
    }

    @Override
    public Tuple getLocalNormalAt(Tuple localPoint) {
        return localPoint.subtract(Tuple.point(0, 0, 0));
    }

    @Override
    public Intersections getLocalIntersections(Ray transformedRay) {
        Tuple sphereCenter = Tuple.point(0, 0, 0);
        Tuple sphereToRay = transformedRay.getOrigin().subtract(sphereCenter);
        double a = transformedRay.getDirection().dot(transformedRay.getDirection());
        double b = 2 * transformedRay.getDirection().dot(sphereToRay);
        double c = sphereToRay.dot(sphereToRay) - 1;

        double discriminant = b * b - 4 * a * c;

        if (discriminant < 0) {
            return new Intersections();
        }

        return new Intersections(
                new Intersection((-b - Math.sqrt(discriminant)) / (2 * a), this),
                new Intersection((-b + Math.sqrt(discriminant)) / (2 * a), this)
        );
    }

}
