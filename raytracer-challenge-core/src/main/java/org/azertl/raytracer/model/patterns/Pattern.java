package org.azertl.raytracer.model.patterns;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;

public abstract class Pattern {
    private Transformation transform = new Transformation();

    public Transformation getTransform() {
        return transform;
    }

    public Pattern setTransform(Transformation transformation) {
        this.transform = transformation;
        return this;
    }

    public abstract Color getPatternAtShape(Shape shape, Tuple point);
}
