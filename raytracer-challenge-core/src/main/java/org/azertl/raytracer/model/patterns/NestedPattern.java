package org.azertl.raytracer.model.patterns;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

public class NestedPattern extends Pattern {
    private Pattern pattern1, pattern2;
    public NestedPattern(Pattern pattern1, Pattern pattern2) {
        super();
        this.pattern1 = pattern1;
        this.pattern2 = pattern2;
    }

    @Override
    public Color getPatternAtShape(Shape shape, Tuple point) {
        if ((Math.floor(point.x) + Math.floor(point.y) + Math.floor(point.z)) % 2 == 0) {
            return pattern1.getPatternAtShape(shape, point);
        }
        return pattern2.getPatternAtShape(shape, point);
    }
}
