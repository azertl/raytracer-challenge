package org.azertl.raytracer.model;

public class Canvas {
    public final int width, height;

    private final Color[][] pixels;

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;

        this.pixels = new Color[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixels[i][j] = new Color(0, 0, 0);
            }
        }
    }

    public Color getPixel(int i, int j) {
        return pixels[i][j];
    }

    public void setPixel(int i, int j, Tuple tuple) {
        pixels[i][j] = new Color(tuple);
    }
}
