package org.azertl.raytracer.model;

public class Ray {
    private final Tuple origin;

    private final Tuple direction;

    public Ray(Tuple origin, Tuple direction) {
        this.origin = origin;
        this.direction = direction;
    }

    public Tuple getOrigin() {
        return origin;
    }

    public Tuple getDirection() {
        return direction;
    }

    public Tuple getPosition(double t) {
        return origin.add(direction.multiply(t));
    }

    public Ray transform(Matrix matrix) {
        return new Ray(
                matrix.multiply(origin),
                matrix.multiply(direction)
        );
    }

    public Intersections intersect(World world) {
        Intersections intersections = new Intersections();
        world.getObjects().forEach(
                (sphere -> intersections.addAll(sphere.intersect(this)))
        );

        return intersections;
    }
}
