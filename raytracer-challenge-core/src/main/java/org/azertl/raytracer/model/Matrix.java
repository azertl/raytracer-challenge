package org.azertl.raytracer.model;

import org.azertl.raytracer.operations.Matrices;
import org.azertl.raytracer.utils.NumberUtils;

import java.util.Arrays;

public class Matrix {
    private final double[][] matrix;

    public Matrix(int rows, int columns) {
        matrix = new double[rows][columns];
    }

    public double get(int i, int j) {
        return matrix[i][j];
    }

    public void set(int i, int j, double value) {
        matrix[i][j] = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Matrix{\n");
        for (int i = 0; i < rows(); i++) {
            sb.append("{");
            boolean first = true;
            for (int j = 0; j < cols(); j++) {
                if (!first) {
                    sb.append(", ");
                }
                sb.append(get(i, j));
                first = false;
            }
            sb.append("}\n");
        }
        sb.append("}");

        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Matrix)) return false;

        Matrix otherMatrix = (Matrix) o;

        if (rows() != otherMatrix.rows() || cols() != otherMatrix.cols()) {
            return false;
        }

        for (int i = 0; i < rows(); i++) {
            for (int j = 0; j < cols(); j++) {
                if (!NumberUtils.equalsDouble(get(i, j), otherMatrix.get(i, j)))
                    return false;
            }
        }

        return true;
    }

    public int cols() {
        return matrix[0].length;
    }

    public int rows() {
        return matrix.length;
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(matrix);
    }

    public Tuple multiply(Tuple tuple) {
        Matrix matrixB = new Matrix(4, 1);
        matrixB.set(0, 0, tuple.x);
        matrixB.set(1, 0, tuple.y);
        matrixB.set(2, 0, tuple.z);
        matrixB.set(3, 0, tuple.w);

        Matrix matrixResult = this.multiply(matrixB);

        return new Tuple(
                matrixResult.get(0, 0),
                matrixResult.get(1, 0),
                matrixResult.get(2, 0),
                matrixResult.get(3, 0)
        );
    }

    public Matrix inverse() {
        double det = Matrices.determinant(this);
        Matrix inversed = new Matrix(cols(),rows());
        for (int i = 0; i < inversed.rows(); i++) {
            for (int j = 0; j < inversed.cols(); j++) {
                double cofactor = Matrices.cofactor(this, j, i);

                inversed.set(i, j, cofactor / det);
            }
        }

        return inversed;
    }

    public Matrix multiply(double scalar) {
        Matrix result = new Matrix(rows(), cols());
        for (int i = 0; i < rows(); i++) {
            for (int j = 0; j < cols(); j++) {
                result.set(i, j, get(i, j) * scalar);
            }
        }
        return result;
    }

    public Matrix multiply(Matrix other) {
        Matrix result = new Matrix(rows(), other.cols());

        for (int i = 0; i < result.rows(); i++) {
            for (int j = 0; j < result.cols(); j++) {
                double sum = 0;

                for (int idx = 0; idx < cols(); idx++) {
                    sum += get(i, idx) * other.get(idx, j);
                }

                result.set(i, j, sum);
            }
        }

        return result;
    }

    public Matrix transpose() {
        Matrix transposed = new Matrix(cols(), rows());

        for (int i = 0; i < transposed.rows(); i++) {
            for (int j = 0; j < transposed.cols(); j++) {
                transposed.set(i, j, get(j, i));
            }
        }

        return transposed;
    }

    public Matrix submatrix(int row, int col) {
        Matrix sub = new Matrix(rows() - 1, cols() - 1);

        // corner upper left
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                sub.set(i, j, get(i, j));
            }
        }

        // corner upper right
        for (int i = 0; i < row; i++) {
            for (int j = col + 1; j < cols(); j++) {
                sub.set(i, j - 1, get(i, j));
            }
        }

        // corner lower left
        for (int i = row + 1; i < rows(); i++) {
            for (int j = 0; j < col; j++) {
                sub.set(i - 1, j, get(i, j));
            }
        }

        // corner lower right
        for (int i = row + 1; i < rows(); i++) {
            for (int j = col + 1; j < cols(); j++) {
                sub.set(i - 1, j - 1, get(i, j));
            }
        }

        return sub;
    }
}
