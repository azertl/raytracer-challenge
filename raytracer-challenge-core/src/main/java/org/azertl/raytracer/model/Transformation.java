package org.azertl.raytracer.model;

import org.azertl.raytracer.operations.Transformations;

// TODO is it really useful to keep this class ?
public final class Transformation extends Matrix {

    public Transformation() {
        super(4, 4);

        set(0, 0, 1);
        set(1, 1, 1);
        set(2, 2, 1);
        set(3, 3, 1);
    }

    public Transformation translation(double x, double y, double z) {
        return getTransformation(Transformations.translation(x, y, z));
    }

    public Transformation scaling(double x, double y, double z) {
        return getTransformation(Transformations.scaling(x, y, z));
    }

    public Transformation rotationX(double radians) {
        return getTransformation(Transformations.rotationX(radians));
    }

    public Transformation rotationY(double radians) {
        return getTransformation(Transformations.rotationY(radians));
    }

    public Transformation rotationZ(double radians) {
        return getTransformation(Transformations.rotationZ(radians));
    }

    public Transformation shearing(double xy, double xz, double yx, double yz, double zx, double zy) {
        return getTransformation(Transformations.shearing(xy, xz, yx, yz, zx, zy));
    }

    public Tuple apply(Tuple tuple) {
        return this.multiply(tuple);
    }

    private Transformation getTransformation(Matrix x) {
        Matrix result = x.multiply(this);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                set(i, j, result.get(i, j));
            }
        }

        return this;
    }
}
