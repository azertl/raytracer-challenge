package org.azertl.raytracer.model.patterns;

import org.azertl.raytracer.model.Color;
import org.azertl.raytracer.model.Shape;
import org.azertl.raytracer.model.Tuple;

// TODO Why does my checkered sphere look weird ? => check UV mapping or spherical texture mapping
public class CheckersPattern extends Pattern {
    private Color color1, color2;

    public CheckersPattern(Color color1, Color color2) {
        super();
        this.color1 = color1;
        this.color2 = color2;
    }

    @Override
    public Color getPatternAtShape(Shape shape, Tuple point) {
        if ((Math.floor(point.x) + Math.floor(point.y) + Math.floor(point.z)) % 2 == 0) {
            return color1;
        }
        return color2;
    }
}
