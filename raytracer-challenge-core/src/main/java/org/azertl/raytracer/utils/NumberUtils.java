package org.azertl.raytracer.utils;

public final class NumberUtils {
    public static final double EPSILON = 0.00001;

    private NumberUtils() {}

    public static boolean equalsDouble(double double1, double double2) {
        return Math.abs(double1 - double2) < EPSILON;
    }
}
