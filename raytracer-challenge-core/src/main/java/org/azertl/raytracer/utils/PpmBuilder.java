package org.azertl.raytracer.utils;

import org.azertl.raytracer.model.Canvas;
import org.azertl.raytracer.model.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.function.Function;

public class PpmBuilder {
    private static final Logger LOG = LoggerFactory.getLogger(PpmBuilder.class);

    private Canvas canvas;

    private StringBuilder ppmBuilder;
    private StringBuilder lineBuilder;

    private String ppmStr;

    public PpmBuilder(Canvas canvas) {
        this.canvas = canvas;
    }

    public String build() {
        if (ppmStr != null) {
            return ppmStr;
        }

        ppmBuilder = new StringBuilder("P3\n" + canvas.width + " " + canvas.height + "\n" + 255 + "\n");

        for (int i = 0; i < canvas.height; i++) {
            if (i > 0) {
                ppmBuilder.append("\n");
            }

            lineBuilder = new StringBuilder();

            boolean isFirstEltInLine = true;
            for (int j = 0; j < canvas.width; j++) {
                Color color = canvas.getPixel(j, i);

                writePpmComposite(color, isFirstEltInLine, Color::red);
                writePpmComposite(color, false, Color::green);
                writePpmComposite(color, false, Color::blue);

                isFirstEltInLine = false;
            }

            ppmBuilder.append(lineBuilder);
        }

        ppmBuilder.append("\n");

        ppmStr = ppmBuilder.toString();

        return ppmStr;
    }

    public boolean save(String path) {
        if (ppmStr == null) {
            build();
        }

        try (FileWriter fw = new FileWriter(path)) {
            fw.write(ppmStr);
        } catch(IOException ioe) {
            LOG.error(ioe.getMessage());
            return false;
        }

        return true;
    }

    private void writePpmComposite(Color color, boolean isFirstEltInLine, Function<Color, Double> meth) {
        int val = (int) (Math.ceil(meth.apply(color) * 255));
        if (val < 0) val = 0;
        if (val > 255) val = 255;

        StringBuilder compositeBuilder = new StringBuilder();
        if (!isFirstEltInLine) compositeBuilder.append(" ");
        compositeBuilder.append(val);

        if (lineBuilder.length() + compositeBuilder.length() > 70) {
            ppmBuilder.append(lineBuilder).append("\n");

            lineBuilder = new StringBuilder();
            lineBuilder.append(val);
        } else {
            lineBuilder.append(compositeBuilder);
        }
    }
}
