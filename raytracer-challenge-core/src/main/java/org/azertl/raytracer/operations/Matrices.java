package org.azertl.raytracer.operations;

import org.azertl.raytracer.model.Matrix;

public final class Matrices {
    private Matrices() {
    }

    public static Matrix identityMatrix() {
        Matrix idMatrix = new Matrix(4, 4);
        idMatrix.set(0, 0, 1);
        idMatrix.set(1, 1, 1);
        idMatrix.set(2, 2, 1);
        idMatrix.set(3, 3, 1);
        return idMatrix;
    }

    public static double minor(Matrix matrix, int row, int col) {
        return determinant(matrix.submatrix(row, col));
    }

    public static double determinant(Matrix matrix) {
        if (matrix.rows() == 2 && matrix.cols() == 2) {
            return matrix.get(0, 0) * matrix.get(1, 1) - matrix.get(0, 1) * matrix.get(1, 0);
        }

        double sum = 0;
        for (int i = 0; i < matrix.rows(); i++) {
            sum += matrix.get(i, 0) * cofactor(matrix, i, 0);
        }
        return sum;
    }

    public static double cofactor(Matrix matrix, int row, int col) {
        return ((row + col) % 2 == 1 ? -1 : 1)
                * minor(matrix, row, col);
    }

    public static boolean isInvertible(Matrix matrix) {
        return determinant(matrix) != 0;
    }

}
