package org.azertl.raytracer.operations;

import org.azertl.raytracer.model.Matrix;
import org.azertl.raytracer.model.Transformation;
import org.azertl.raytracer.model.Tuple;

public final class Transformations {
    private Transformations() {
    }

    public static Matrix translation(double x, double y, double z) {
        Matrix translation = Matrices.identityMatrix();

        translation.set(0, 3, x);
        translation.set(1, 3, y);
        translation.set(2, 3, z);
        return translation;
    }

    public static Matrix scaling(double x, double y, double z) {
        Matrix translation = Matrices.identityMatrix();

        translation.set(0, 0, x);
        translation.set(1, 1, y);
        translation.set(2, 2, z);
        return translation;
    }

    public static Matrix rotationX(double radians) {
        Matrix rotation = Matrices.identityMatrix();

        rotation.set(1, 1, Math.cos(radians));
        rotation.set(1, 2, -Math.sin(radians));
        rotation.set(2, 1, Math.sin(radians));
        rotation.set(2, 2, Math.cos(radians));

        return rotation;
    }

    public static Matrix rotationY(double radians) {
        Matrix rotation = Matrices.identityMatrix();

        rotation.set(0, 0, Math.cos(radians));
        rotation.set(0, 2, Math.sin(radians));
        rotation.set(2, 0, -Math.sin(radians));
        rotation.set(2, 2, Math.cos(radians));

        return rotation;
    }

    public static Matrix rotationZ(double radians) {
        Matrix rotation = Matrices.identityMatrix();

        rotation.set(0, 0, Math.cos(radians));
        rotation.set(0, 1, -Math.sin(radians));
        rotation.set(1, 0, Math.sin(radians));
        rotation.set(1, 1, Math.cos(radians));

        return rotation;
    }

    public static Matrix shearing(double xy, double xz, double yx, double yz, double zx, double zy) {
        Matrix shearing = Matrices.identityMatrix();

        shearing.set(0, 1, xy);
        shearing.set(0, 2, xz);

        shearing.set(1, 0, yx);
        shearing.set(1, 2, yz);

        shearing.set(2, 0, zx);
        shearing.set(2, 1, zy);

        return shearing;
    }

    public static Matrix viewTransform(Tuple from, Tuple to, Tuple up) {
        Tuple forward = to.subtract(from).normalize();
        Tuple left = Tuples.cross(forward, up.normalize());
        Tuple trueUp = Tuples.cross(left, forward);

        Matrix orientation = new Matrix(4, 4);
        orientation.set(0, 0, left.x);
        orientation.set(0, 1, left.y);
        orientation.set(0, 2, left.z);
        orientation.set(1, 0, trueUp.x);
        orientation.set(1, 1, trueUp.y);
        orientation.set(1, 2, trueUp.z);
        orientation.set(2, 0, -forward.x);
        orientation.set(2, 1, -forward.y);
        orientation.set(2, 2, -forward.z);
        orientation.set(3, 3, 1);

        Matrix translation = Transformations.translation(-from.x, -from.y, -from.z);
        return orientation.multiply(translation);
    }
}
