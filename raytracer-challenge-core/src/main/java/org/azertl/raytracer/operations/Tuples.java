package org.azertl.raytracer.operations;

import org.azertl.raytracer.model.Tuple;

public final class Tuples {

    public static Tuple divide(Tuple tuple, double scalar) {
        return new Tuple(tuple.x / scalar, tuple.y / scalar, tuple.z / scalar, tuple.w / scalar);
    }

    public static Tuple cross(Tuple tuple1, Tuple tuple2) {
        return Tuple.vector(
                tuple1.y * tuple2.z - tuple1.z * tuple2.y,
                tuple1.z * tuple2.x - tuple1.x * tuple2.z,
                tuple1.x * tuple2.y - tuple1.y * tuple2.x
        );
    }

}
